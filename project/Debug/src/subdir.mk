################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ComparabilityGraphChecker.cpp \
../src/NestedMapping.cpp \
../src/OPPSolver.cpp \
../src/nested_loop_mapper.cpp 

OBJS += \
./src/ComparabilityGraphChecker.o \
./src/NestedMapping.o \
./src/OPPSolver.o \
./src/nested_loop_mapper.o 

CPP_DEPS += \
./src/ComparabilityGraphChecker.d \
./src/NestedMapping.d \
./src/OPPSolver.d \
./src/nested_loop_mapper.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


