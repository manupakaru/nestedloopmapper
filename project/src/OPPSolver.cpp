/*
 * OPPSolver.cpp
 *
 *  Created on: 23 Oct 2017
 *      Author: manupa
 */

#include "OPPSolver.h"
#include <chrono>
#include <ctime>

#define TIME_LIMIT 200


OPPSolver::OPPSolver(std::set<node, node_compare> V,
		std::map<node, int, node_compare> Wx,
		std::map<node, int, node_compare> Wy,
		std::map<node, int, node_compare> Wz) {

	this->V = V;
	W[X]=Wx;W[Y]=Wy;W[Z]=Wz;

}

bool OPPSolver::checkP3(Aug augIns, std::queue<Aug>* L) {
//	std::cout << "CheckP3 started.\n";
//	assert(false);

//	std::cout << "aug : " << "(" << augIns.edge.n1.idx << "," << augIns.edge.n2.idx << "),";
//	std::cout << augIns.isPlus << ",";
//	if(augIns.dim==X)std::cout << "X\n";
//	if(augIns.dim==Y)std::cout << "Y\n";
//	if(augIns.dim==Z)std::cout << "Z\n";


	if(!augIns.isPlus){
		//std::cout << "P3 : true\n";
		//std::cout << "CheckP3 done.\n";
		return true;
	}

	std::set<DIM> F;
	for (std::pair<DIM,std::set<udEdge,udEdge_compare>> pair1 : E_plus_bar){
		////if(pair1.first==X)std::cout << "X\n";
		////if(pair1.first==Y)std::cout << "Y\n";
		////if(pair1.first==Z)std::cout << "Z\n";
		auto currDimEdges = pair1.second;
		if(currDimEdges.find(augIns.edge)!=currDimEdges.end()){
			F.insert(pair1.first);
		}
	}

	if(F.empty()){
//		std::cout << "P3 : false\n";
//		std::cout << "CheckP3 done.\n";
		return false;
	}
	else if(F.size()==1){
		if(E_minus[*F.begin()].find(augIns.edge)==E_minus[*F.begin()].end()){
			insertEMinus(*F.begin(),augIns.edge);
			L->push(Aug(augIns.edge,false,*F.begin()));
		}
	}

	//std::cout << "P3 : true\n";
	//std::cout << "CheckP3 done.\n";
	return true;
}

bool OPPSolver::avoidClique(Aug currAug) {

	DIM d = currAug.dim;
	node b = currAug.edge.n1;
	node c = currAug.edge.n2;

	std::set<node,node_compare> S0_V;
	std::map<node,int,node_compare> S0_W;
	std::set<udEdge,udEdge_compare> S0_E;
	if(!currAug.isPlus){
		for(node v : V){
			if(b.idx == v.idx) continue;
			if(c.idx == v.idx) continue;

			udEdge searchEdge1 = udEdge(b,v);
			udEdge searchEdge2 = udEdge(c,v);

			if((E_minus[d].find(searchEdge1)!=E_minus[d].end()) &&
			   (E_minus[d].find(searchEdge2)!=E_minus[d].end())	){
				S0_V.insert(v);
				S0_W[v]=W[d][v];
			}
		}

		for(node n1 : S0_V){
			for(node n2 : S0_V){
				if(n1.idx==n2.idx)continue;
				udEdge searchEdge1 = udEdge(n1,n2);
				if(E_minus[d].find(searchEdge1)!=E_minus[d].end()){
					S0_E.insert(searchEdge1);
				}
			}
		}

		ComparabilityGraphChecker CGCIns;
		CGCIns.reset();
		CGCIns.setV(S0_V,S0_W);
		CGCIns.setE(S0_E);
		std::set<std::pair<int,int>> cycle = CGCIns.get2ChordLessCycle();
		if(cycle.empty()){
			std::set<node,node_compare> maxWClq = CGCIns.getMaxWeightedClique();
			int totalW=0;

			//std::cout << "maxclq nodes : \n";
			for (node n : maxWClq){
				//std::cout << n.idx << ",";
				totalW += W[d][n];
			}

			totalW += W[d][b] + W[d][c];

			if(totalW > limits[d]){
//				std::cout << "avoided a clique!\n";
				return false;
			}
		}
	}

	return true;
}

void OPPSolver::insertEPlus(DIM dim, udEdge e) {
	//std::cout << "insertEPLus started : \n";
	if(dim==X){
		//std::cout << "X\n";
    }
	if(dim==Y){
		//std::cout << "Y\n";
	}
	if(dim==Z){
		//std::cout << "Z\n";
	}
	//std::cout << "edge : (" << e.n1.idx << "," << e.n2.idx << ")\n";
	assert(E_plus_bar[dim].find(e)!=E_plus_bar[dim].end());
	E_plus_bar[dim].erase(E_plus_bar[dim].find(e));
	E_plus[dim].insert(e);

	//std::cout << "insertEPLus end : \n";
}

void OPPSolver::insertEMinus(DIM dim, udEdge e) {
	//std::cout << "insertEMinus started : \n";
	if(dim==X){
		//std::cout << "X\n";
    }
	if(dim==Y){
		//std::cout << "Y\n";
	}
	if(dim==Z){
		//std::cout << "Z\n";
	}
	//std::cout << "edge : (" << e.n1.idx << "," << e.n2.idx << ")\n";

	assert(E_minus_bar[dim].find(e)!=E_minus_bar[dim].end());
	E_minus_bar[dim].erase(E_minus_bar[dim].find(e));
	E_minus[dim].insert(e);

	//std::cout << "insertEMinus end : \n";
}

bool OPPSolver::avoidC4(Aug currAug, std::queue<Aug>* L) {

	ComparabilityGraphChecker CGCIns;
	DIM currDIM = currAug.dim;
	//std::cout << "currDIM :"; printDIM(currDIM);

	if(currAug.isPlus){

		CGCIns.reset();
		CGCIns.setV(V,W[currDIM]);

//		validateEdges();

		{
		std::set<udEdge,udEdge_compare> fmods;
			for(udEdge f : E_minus_bar[currDIM]){

//				validateEdges();

////				std::cout << "f.n1.idx=" << f.n1.idx << ",f.n2.idx=" << f.n2.idx << "\n";
				assert(f.n1.idx != 0);
				assert(f.n2.idx != 0);

				CGCIns.setE(E_plus[currDIM]);
				CGCIns.addUndirectedEdge(f);

				std::set<std::pair<int,int>> c4Chords = CGCIns.getC4Chords();
				if(!c4Chords.empty()){
					std::set<udEdge, udEdge_compare> ude_cycle;
					std::set<udEdge, udEdge_compare> ude_chords;
					convertC4(c4Chords,&ude_cycle,&ude_chords);

					assert(ude_chords.size()==2);
					udEdge chord1 = *ude_chords.begin();
					udEdge chord2 = *(ude_chords.begin()++);

					if( (ude_cycle.find(currAug.edge)!=ude_cycle.end()) &&
						 E_minus[currDIM].find(chord1)!=E_minus[currDIM].end() &&
						 E_minus[currDIM].find(chord2)!=E_minus[currDIM].end()){

						if(E_plus[currDIM].find(f)!=E_plus[currDIM].end()){
							return false;
						}
	//					insertEMinus(currDIM,f);
						fmods.insert(f);
						Aug fAug(f,false,currDIM);
						L->push(fAug);
					}

				}
			}
			for (udEdge e : fmods){
				insertEMinus(currDIM,e);
			}
		}

		CGCIns.reset();
		CGCIns.setV(V,W[currDIM]);

		{
		std::set<udEdge,udEdge_compare> fmods;
			for(udEdge f : E_plus_bar[currDIM]){
				CGCIns.setE(E_plus[currDIM]);
				std::set<std::pair<int,int>> c4Chords = CGCIns.getC4Chords();

				if(!c4Chords.empty()){
					std::set<udEdge, udEdge_compare> ude_cycle;
					std::set<udEdge, udEdge_compare> ude_chords;
					convertC4(c4Chords,&ude_cycle,&ude_chords);

					assert(ude_chords.size()==2);
					udEdge chord1 = *ude_chords.begin();
					udEdge chord2 = *(ude_chords.begin()++);
					udEdge& otherChord=chord2;

					if(chord1==f){
						otherChord=chord2;
					}
					else if (chord2==f){
						otherChord=chord1;
					}
					else{
						continue;
					}

					if( (ude_cycle.find(currAug.edge)!=ude_cycle.end()) &&
						 E_minus[currDIM].find(otherChord)!=E_minus[currDIM].end()){

						if(E_minus[currDIM].find(f)!=E_minus[currDIM].end()){
							return false;
						}
//						insertEPlus(currDIM,f);
						fmods.insert(f);
						Aug fAug(f,true,currDIM);
						L->push(fAug);
					}
				}
			}
			for (udEdge e : fmods){
				insertEPlus(currDIM,e);
			}
		}

	}
	else{

		CGCIns.reset();
		CGCIns.setV(V,W[currDIM]);

		{
		std::set<udEdge,udEdge_compare> fmods;
			for(udEdge f : E_minus_bar[currDIM]){
				CGCIns.setE(E_plus[currDIM]);
				CGCIns.addUndirectedEdge(f);

				std::set<std::pair<int,int>> c4Chords = CGCIns.getC4Chords();
				if(!c4Chords.empty()){
					std::set<udEdge, udEdge_compare> ude_cycle;
					std::set<udEdge, udEdge_compare> ude_chords;
					convertC4(c4Chords,&ude_cycle,&ude_chords);

					assert(ude_chords.size()==2);
					udEdge chord1 = *ude_chords.begin();
					udEdge chord2 = *(ude_chords.begin()++);
					udEdge& otherChord=chord2;

					if(chord1==currAug.edge){
						otherChord=chord2;
					}
					else if (chord2==currAug.edge){
						otherChord=chord1;
					}
					else{
						continue;
					}

					if( (ude_cycle.find(currAug.edge)!=ude_cycle.end()) &&
						 E_minus[currDIM].find(otherChord)!=E_minus[currDIM].end()){

						if(E_plus[currDIM].find(f)!=E_plus[currDIM].end()){
							return false;
						}

//						insertEMinus(currDIM,f);
						fmods.insert(f);
						Aug fAug(f,false,currDIM);
						L->push(fAug);
					}

				}


			}
			for (udEdge e : fmods){
				insertEMinus(currDIM,e);
			}
		}


		CGCIns.reset();
		CGCIns.setV(V,W[currDIM]);


		{
		std::set<udEdge,udEdge_compare> fmods;
			for(udEdge f : E_plus_bar[currDIM]){
				CGCIns.setE(E_plus[currDIM]);
				std::set<std::pair<int,int>> c4Chords = CGCIns.getC4Chords();

				if(!c4Chords.empty()){
					std::set<udEdge, udEdge_compare> ude_cycle;
					std::set<udEdge, udEdge_compare> ude_chords;
					convertC4(c4Chords,&ude_cycle,&ude_chords);

					assert(ude_chords.size()==2);
					udEdge chord1 = *ude_chords.begin();
					udEdge chord2 = *(ude_chords.begin()++);
					udEdge& otherChord=chord2;

					if(chord1==currAug.edge){
						otherChord=chord2;
					}
					else if (chord2==currAug.edge){
						otherChord=chord1;
					}
					else{
						continue;
					}

					if(otherChord==f){
						if(E_minus[currDIM].find(f)!=E_minus[currDIM].end()){
							return false;
						}
//						insertEPlus(currDIM,f);
						fmods.insert(f);
						Aug fAug(f,true,currDIM);
						L->push(fAug);
					}
				}
			}
			for (udEdge e : fmods){
				insertEPlus(currDIM,e);
			}
		}
	}
	return true;
}

void OPPSolver::insertN(node N, int xN, int yN, int zN) {
	V.insert(N);
	W[X][N]=xN;
	W[Y][N]=yN;
	W[Z][N]=zN;
}

void OPPSolver::printDIM(DIM d) {
	//if(d==X)std::cout << "X\n";
	//if(d==Y)std::cout << "Y\n";
	//if(d==Z)std::cout << "Z\n";
}

int OPPSolver::printEdges() {

	std::cout << "The Nodes :\n";
	int vol=0;
	for(node n : V){
		std::cout << n.idx << ":" << n.name << ",(" << W[X][n] << "," << W[Y][n] << "," <<  W[Z][n] << ")\n";
		vol+=W[X][n]*W[Y][n]*W[X][n];
	}
	std::cout << "\n";
	std::cout << "vol=" << vol << "\n";

	std::cout << "E_plusX edges : " << E_plus[X].size() << "::";
	for(udEdge e : E_plus[X]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	std::cout << "E_plusY edges : " << E_plus[Y].size() << "::";
	for(udEdge e : E_plus[Y]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	std::cout << "E_plusZ edges : " << E_plus[Z].size() << "::";
	for(udEdge e : E_plus[Z]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	std::cout << "E_minusX edges : " << E_minus[X].size() << "::";
	for(udEdge e : E_minus[X]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	std::cout << "E_minusY edges : " << E_minus[Y].size() << "::";
	for(udEdge e : E_minus[Y]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	std::cout << "E_minusZ edges : " << E_minus[Z].size() << "::";
	for(udEdge e : E_minus[Z]){
		std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}
	std::cout << "\n";

	return vol;
}


void OPPSolver::convertC4(std::set<std::pair<int, int> > c4Chords,
		std::set<udEdge, udEdge_compare>* ude_cycle,
		std::set<udEdge, udEdge_compare>* ude_chords) {

	std::vector<node> chordNodes;
	for(std::pair<int, int> pair : c4Chords){
		chordNodes.push_back(node(pair.first));
		chordNodes.push_back(node(pair.second));
	}
	assert(chordNodes.size()==4);

	ude_chords->insert(udEdge(chordNodes[0],chordNodes[1]));
	ude_chords->insert(udEdge(chordNodes[2],chordNodes[3]));

	ude_cycle->insert(udEdge(chordNodes[0],chordNodes[2]));
	ude_cycle->insert(udEdge(chordNodes[0],chordNodes[3]));
	ude_cycle->insert(udEdge(chordNodes[1],chordNodes[2]));
	ude_cycle->insert(udEdge(chordNodes[1],chordNodes[3]));
}

searchInfo OPPSolver::getCurrSearchInfo(Aug aug) {
	searchInfo si(E_plus,E_plus_bar,E_minus,E_minus_bar,aug);
	return si;
}

void OPPSolver::setCurrSearchInfo(searchInfo si) {
	E_minus=si.E_minus;
	E_minus_bar=si.E_minus_bar;
	E_plus=si.E_plus;
	E_plus_bar=si.E_plus_bar;
}

void OPPSolver::initSearchInfo() {
	E_minus.clear();
	E_minus_bar.clear();
	E_plus.clear();
	E_plus_bar.clear();

	for(node n1 : V){
		for (node n2 : V){
			if(n1.idx==n2.idx)continue;
			assert(n1.idx != 0);
			assert(n2.idx != 0);
			E_minus_bar[X].insert(udEdge(n1,n2));
			E_minus_bar[Y].insert(udEdge(n1,n2));
			E_minus_bar[Z].insert(udEdge(n1,n2));
			E_plus_bar[X].insert(udEdge(n1,n2));
		    E_plus_bar[Y].insert(udEdge(n1,n2));
			E_plus_bar[Z].insert(udEdge(n1,n2));
		}
	}

	while(!searchInfoQ.empty()){
		searchInfoQ.pop();
	}

	searchInfoQ.push(searchInfo(E_minus,E_minus_bar,E_plus,E_plus_bar,Aug(true)));

	E_minus_bar.clear();
	E_plus_bar.clear();
}

std::queue<Aug> OPPSolver::initAug() {
	//std::cout << "initAug Started : \n";

	std::queue<Aug> resQ;

	for (node n1 : V){
		for(node n2 : V){
			if(n1.idx==n2.idx)continue;
			if(W[X][n1] + W[X][n2] > limits[X]){
				if(E_plus[X].find(udEdge(n1,n2))==E_plus[X].end()){
					//std::cout << "Xdim : adding edge : " << n1.idx << "," << n2.idx << "\n";
					insertEPlus(X,udEdge(n1,n2));
					resQ.push(Aug(udEdge(n1,n2),true,X));
				}
			}
			if(W[Y][n1] + W[Y][n2] > limits[Y]){
				if(E_plus[Y].find(udEdge(n1,n2))==E_plus[Y].end()){
					//std::cout << "Ydim : adding edge : " << n1.idx << "," << n2.idx << "\n";
					insertEPlus(Y,udEdge(n1,n2));
					resQ.push(Aug(udEdge(n1,n2),true,Y));
				}
			}
			if(W[Z][n1] + W[Z][n2] > limits[Z]){
				if(E_plus[Z].find(udEdge(n1,n2))==E_plus[Z].end()){
					//std::cout << "Zdim : adding edge : " << n1.idx << "," << n2.idx << "\n";
					insertEPlus(Z,udEdge(n1,n2));
					resQ.push(Aug(udEdge(n1,n2),true,Z));
				}
			}
		}
	}

//	assert(resQ.empty());
	//std::cout << "initAug end : \n";
	return resQ;
}

bool OPPSolver::updateSearchInfo(Aug currAug) {

	std::queue<Aug> L;

	if(currAug.init){
		L=initAug();
	}
	else{
		if(currAug.isPlus){
			insertEPlus(currAug.dim,currAug.edge);
			L.push(currAug);
		}
		else{
			//TODO : add edges that cannot be distinuished
			insertEMinus(currAug.dim,currAug.edge);
			L.push(currAug);
		}
	}

	while(!L.empty()){
//		assert(false);
		Aug augL = L.front();
		L.pop();

//		validateEdges();

		if(checkP3(augL,&L)==false)  return false;
		if(avoidC4(augL,&L)==false)  return false;
		if(avoidClique(augL)==false) return false;

	}

	return true;
}

std::string OPPSolver::packingClassTest(Aug* currAug) {
	std::set<DIM> dims;
	dims.insert(X);dims.insert(Y);dims.insert(Z);
	//std::cout << "packingClassTest started.\n";

	std::set<udEdge,udEdge_compare> A;
	ComparabilityGraphChecker CGCIns;

	for(DIM d : dims){
		//std::cout << "DIM : ";
		printDIM(d);

		CGCIns.reset();
		CGCIns.setV(V,W[d]);
		CGCIns.setE(E_plus_bar[d]);
		std::set<std::pair<int,int>> cycle = CGCIns.get2ChordLessCycle();
		if(!cycle.empty()){
			//std::cout << "2ChordLesscycle found\n";
			for(std::pair<int,int> pair : cycle){
				A.insert(udEdge(CGCIns.getNode(pair.first),CGCIns.getNode(pair.second)));
			}
		}
		else{
			//std::cout << "2ChordLesscycle cycle NOT found\n";
			std::set<node,node_compare> maxWClq = CGCIns.getMaxWeightedClique();
			int totalW=0;

			//std::cout << "maxclq nodes : \n";
			for (node n : maxWClq){
				//std::cout << n.idx << ",";
				totalW += W[d][n];
			}
			//std::cout << "\ntotalW = " << totalW << "\n";

			if(totalW > limits[d]){
				for(node n1 : maxWClq){
					for(node n2 : maxWClq){
						if(n1.idx==n2.idx)continue;
						A.insert(udEdge(n1,n2));
					}
				}
			}
			else{
				CGCIns.reset();
				CGCIns.setV(V,W[d]);
				CGCIns.setE(E_plus[d]);
				std::set<std::pair<int,int>> c4Chords = CGCIns.getC4Chords();

				//TODO:remove
				if(d==Z){
					c4Chords.clear();
				}

				if(!c4Chords.empty()){
					std::set<udEdge, udEdge_compare> ude_cycle;
					std::set<udEdge, udEdge_compare> ude_chords;
					convertC4(c4Chords,&ude_cycle,&ude_chords);

					for(udEdge e : ude_chords){
						//std::cout << "c4chord :"; printE(e);
						assert(E_plus[d].find(e)==E_plus[d].end());
						A.insert(e);
					}
				}
			}
		}

//		if(d==X)std::cout << "X,";
//		if(d==Y)std::cout << "Y,";
//		if(d==Z)std::cout << "Z,";
//		std::cout << "Asize = " << A.size() << "\n";

		while(!A.empty()){
			std::set<udEdge,udEdge_compare> A_reduct_E_minus = A;
			for(udEdge e : A){
				if(E_minus[d].find(e)!=E_minus[d].end()){
					A_reduct_E_minus.erase(e);
				}
			}

		    if(A_reduct_E_minus.empty()){
		    	//std::cout << "packingClassTest done.\n";
		    	return "EXIT";
		    }
		    else{
		    	*currAug=Aug(*A_reduct_E_minus.begin(),true,d);
		    	assert(currAug->edge.n1.idx > 0);
		    	if(A_reduct_E_minus.size()==1){
		    		//std::cout << "packingClassTest done.\n";
		    		return "FIX";
		    	}
		    	else{
		    		//std::cout << "packingClassTest done.\n";
		    		return "BRANCH";
		    	}
		    }
		}

	}
//	assert(false);
	//std::cout << "packingClassTest done.\n";
	return "SUCCESS";
}

bool OPPSolver::solveOPP() {
    auto start = std::chrono::system_clock::now();


	//assuming all the nodes are inserted and the limits have been set
	initSearchInfo();

	//std::cout << "solveOPP for boxes : " << V.size() << "\n";
	for (node n : V){
		if(W[X][n] > limits[X]){
			return false;
		}
		if(W[Y][n] > limits[Y]){
			return false;
		}
		if(W[Z][n] > limits[Z]){
			return false;
		}
//		std::cout << "n:" << n.idx << "," << W[X][n] << "," << W[Y][n] << "," << W[Z][n] << "\n";
	}

	while(!searchInfoQ.empty()){
	    auto end = std::chrono::system_clock::now();
	    std::chrono::duration<double> elapsed_seconds = end-start;
	    if(elapsed_seconds.count() > TIME_LIMIT){
	    	std::cout << "time limit exceeded.\n";
	    	return false;
	    }

//		searchInfo si = searchInfoQ.front();
		searchInfo si = searchInfoQ.top();
		searchInfoQ.pop();
		setCurrSearchInfo(si);


//		std::cout << "In\n";
//		printEdges();

//		validateEdges();

		std::string result;
		do{
			if(updateSearchInfo(si.aug)==false){
				result="EXIT";
			}
			else{
				result=packingClassTest(&si.aug);

				//std::cout << "packclassTest : " << result << "\n";
				//std::cout << "with the edge : \n";
				//std::cout << "edge : (" << si.aug.edge.n1.idx << "," << si.aug.edge.n2.idx << "),";
				printDIM(si.aug.dim);
			}
		}while(result.compare("FIX")==0);

		if(result.compare("SUCCESS")==0){
//			while(!searchInfoQ.empty()){
//				searchInfoQ.pop();
//			}
			searchInfoQ.push(si);
			return true;
		}

		//std::cout << "Out\n";
//		printEdges();

		if(result.compare("BRANCH")==0){
			searchInfoQ.push(getCurrSearchInfo(Aug(si.aug.edge,true,si.aug.dim)));
			searchInfoQ.push(getCurrSearchInfo(Aug(si.aug.edge,false,si.aug.dim)));
		}
	}
	return false;


}

bool OPPSolver::solveOPP_V2(std::set<node,node_compare> V_new,
		                    std::map<node,int,node_compare> Wx_new,
		                    std::map<node,int,node_compare> Wy_new,
		                    std::map<node,int,node_compare> Wz_new) {
//	//assuming all the nodes are inserted and the limits have been set
//	initSearchInfo();

    auto start = std::chrono::system_clock::now();

	for(node N_new : V_new){
		V.insert(N_new);
		W[X][N_new]=Wx_new[N_new];
		W[Y][N_new]=Wy_new[N_new];
		W[Z][N_new]=Wz_new[N_new];
	}

	//std::cout << "solveOPP for boxes : " << V.size() << "\n";
	for (node n : V_new){
		if(W[X][n] > limits[X]){
			return false;
		}
		if(W[Y][n] > limits[Y]){
			return false;
		}
		if(W[Z][n] > limits[Z]){
			return false;
		}
		//std::cout << "n:" << n.idx << "," << W[X][n] << "," << W[Y][n] << "," << W[Z][n] << "\n";
	}

	assert(!searchInfoQ.empty());

	{
		std::vector<searchInfo> tmpVec;
		while(!searchInfoQ.empty()){
//			searchInfo si = searchInfoQ.front();
			searchInfo si = searchInfoQ.top();
			searchInfoQ.pop();

			bool overlapInAllDirs = true;

			for(node n1 : V_new){
				for(node n2 : V){
					if(n1.idx==n2.idx)continue;
					assert(n1.idx != 0);
					assert(n2.idx != 0);
					si.E_minus_bar[X].insert(udEdge(n1,n2));
					si.E_minus_bar[Y].insert(udEdge(n1,n2));
					si.E_minus_bar[Z].insert(udEdge(n1,n2));

					overlapInAllDirs = true;

					if(W[X][n1] + W[X][n2] > limits[X]){
						si.E_plus[X].insert(udEdge(n1,n2));
					}
					else{
						overlapInAllDirs = false;
						si.E_plus_bar[X].insert(udEdge(n1,n2));
					}

					if(W[Y][n1] + W[Y][n2] > limits[Y]){
						si.E_plus[Y].insert(udEdge(n1,n2));
					}
					else{
						overlapInAllDirs = false;
						si.E_plus_bar[Y].insert(udEdge(n1,n2));
					}

					if(W[Z][n1] + W[Z][n2] > limits[Z]){
						si.E_plus[Z].insert(udEdge(n1,n2));
					}
					else{
						overlapInAllDirs = false;
						si.E_plus_bar[Z].insert(udEdge(n1,n2));
					}

				}
			}
			if(overlapInAllDirs){
				continue;
			}
			tmpVec.push_back(si);
		}

		for(searchInfo si : tmpVec){
			searchInfoQ.push(si);
		}
	}

	while(!searchInfoQ.empty()){
	    auto end = std::chrono::system_clock::now();
	    std::chrono::duration<double> elapsed_seconds = end-start;
	    if(elapsed_seconds.count() > TIME_LIMIT){
	    	std::cout << "time limit exceeded.\n";
	    	return false;
	    }

//		searchInfo si = searchInfoQ.front();
		searchInfo si = searchInfoQ.top();
		searchInfoQ.pop();
		setCurrSearchInfo(si);

		//std::cout << "In\n";
//		printEdges();

//		validateEdges();

		std::string result;
		do{
			if(updateSearchInfo(si.aug)==false){
				result="EXIT";
			}
			else{
				result=packingClassTest(&si.aug);

				//std::cout << "packclassTest : " << result << "\n";
				//std::cout << "with the edge : \n";
				//std::cout << "edge : (" << si.aug.edge.n1.idx << "," << si.aug.edge.n2.idx << "),";
				printDIM(si.aug.dim);
			}
		}while(result.compare("FIX")==0);

		if(result.compare("SUCCESS")==0) {
//			while(!searchInfoQ.empty()){
//				searchInfoQ.pop();
//			}
			searchInfoQ.push(si);
			return true;
		}

		//std::cout << "Out\n";
//		printEdges();

		if(result.compare("BRANCH")==0){
			searchInfoQ.push(getCurrSearchInfo(Aug(si.aug.edge,true,si.aug.dim)));
			searchInfoQ.push(getCurrSearchInfo(Aug(si.aug.edge,false,si.aug.dim)));
		}
	}
	return false;


}

void OPPSolver::validateEdges() {


	for(udEdge e : E_minus_bar[Y]){
		assert(V.find(e.n1)!=V.end());
		assert(V.find(e.n2)!=V.end());
		assert(e.n1.idx!=0);
		assert(e.n2.idx!=0);
		//std::cout << "(" << e.n1.idx << "," << e.n2.idx << "),";
	}



}

void OPPSolver::clearSearchInfoQ() {
	while(!searchInfoQ.empty()){
		searchInfoQ.pop();
	}
}
