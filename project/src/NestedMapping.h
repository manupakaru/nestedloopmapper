/*
 * NestedMapping.h
 *
 *  Created on: 20 Oct 2017
 *      Author: manupa
 */

#ifndef NESTEDMAPPING_H_
#define NESTEDMAPPING_H_

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <queue>
#include <assert.h>

class OPPSolver;

struct ResultNMp{
	int commCycles;
	int CGRACycles;
	int HostCycles;
	int configContexts;
};

#define UNDEFINED 10000

struct node{
	int idx;
	std::string name;
	node(int idx) : idx(idx){};
	node(){};
};

struct node_compare {
    inline bool operator() (const node n1, const node n2) const {
        return n1.idx < n2.idx;
    }
};

struct udEdge{
	node n1; //always the lower idx
	node n2; //always the higher idx
	udEdge(node n1_, node n2_){
		assert(n1_.idx!=n2_.idx);
		if(n1_.idx < n2_.idx){
			n1 = n1_;
			n2 = n2_;
		}
		else{
			n1 = n2_;
			n2 = n1_;
		}

		assert(n1.idx != 0);
		assert(n2.idx != 0);
	}

	bool operator==(const udEdge& other)
	{
	    return (this->n1.idx==other.n1.idx) && (this->n2.idx==other.n2.idx);
	}
};
struct udEdge_compare {
    inline bool operator() (const udEdge e1, const udEdge e2) const {
        return e1.n1.idx<e2.n1.idx || ((e1.n1.idx==e2.n1.idx)&&(e1.n2.idx<e2.n2.idx));
    }
};

struct boxDims{
	int x;
	int y;
	int z;
	boxDims(int x, int y, int z) : x(x), y(y), z(z){};
	boxDims(){}
};

struct searchInfoKS;

enum Strategy{CGRA_CYCLES,GAIN_DENSITY,MIXED};
#define SUBOPTIMAL


class NestedMapping {
public:
	NestedMapping();
	int parseInput(std::string fileName);
	int parseInputType2(std::string fileName);
	int parseInputType3(std::string fileName);
	int parseInputType4(std::string fileName);
	ResultNMp calcCycles(std::map<std::string,std::map<std::string,std::pair<int,int>>> loopsForCGRA);
	ResultNMp calcCyclesType3(std::map<std::string,std::map<std::string,std::pair<int,int>>> loopsForCGRA);

	bool findBestLoops(int XWidth, int YWidth, int ZWidth);
	std::map<std::string,std::set<std::string>> getLoop2MUnitMap(){return Loop2MUnitMap;}
	void populateLoopConstraints();

	void getmUnitCombinations(std::map<std::string,std::vector<boxDims>>& mUnitDims,
							  std::map<std::string,std::vector<boxDims>>::iterator  mUnitDimsIT,
							  std::map<std::string,boxDims> currBoxComb,
							  std::vector<std::map<std::string,boxDims>>* prevCombs=NULL
							  );

	OPPSolver createOPPSolver(std::map<std::string,boxDims> boxConfig, boxDims Container);
	ResultNMp calcCycles(std::set<std::string> L_plus, std::map<std::string,boxDims> mUnitDims);
	void insertLoopISODim(std::string loopName,
						  std::map<std::string,std::map<std::string,std::pair<int,int>>>* loopsForCGRAPtr,
			              int XDim,
						  int YDim);

	std::vector<std::string> sortPotLoop(searchInfoKS currSI);

	Strategy getStrgy() const{return strgy;}
	void setStrgy(Strategy strgy = GAIN_DENSITY){this->strgy = strgy;}

private:
	int fCalls;
		     //LoopName           //LoopName
	std::map<std::string,std::map<std::string,int> > weightedScalarTransEdges;
	std::map<std::string,std::map<std::string,int> > weightedArrTransEdges;

	//For type 2 inputs
	std::map<std::string,std::map<std::string,int>> Munit2MunitInvo;
	std::map<std::string,std::map<std::string,int>> Loop2LoopInvoWeightedScalar;

	std::map<std::string,std::map<std::string,int>> Loop2LoopInvoWeightedArr;
	std::map<std::string,std::map<std::string,int>> Munit2MunitInvoWeightedArr;


	//type 3 inputs
	std::map<std::string,std::map<std::string,std::set<std::string>>> scalarVarSetEdges;
	std::map<std::string,std::map<std::string,std::set<std::string>>> arrVarSetEdges;
	std::map<std::string,int> loopInv;

	std::map<std::string,int> arrSizes;

	float arrayCommDivisor=1;
	float scalarCommDivisor=1;

	int currXWidth=0;
	int currYWidth=0;
	int currZWidth=0;

	std::map<std::string,std::set<std::string> > Loop2MUnitMap;
	std::map<std::string,std::set<std::string> > MUnit2BBMap;
	std::map<std::string,std::string> Munit2LoopMap;
	std::map<std::string,int> BBInvoMap;
	std::map<std::string,int> BBNodeCountMap;
	std::map<std::string,int> MUnitInvoMap;
	std::map<std::string,int> MUnitIterMap;
		     //MUnitName           //X           //Y //II
	std::map<std::string, std::map<int, std::map<int,int> > > MappableBlocks;
	std::map<std::string, std::map<int, std::map<int,int> > > MappableBlocksLatency;
	std::map<std::string, std::set<std::string>> loopConstraints;

	Strategy strgy = GAIN_DENSITY;

	//				  L_plus	   CGRA_Cycles
	std::map<std::set<std::string>,int> leafNodeCGRACycles;




};

#endif /* NESTEDMAPPING_H_ */
