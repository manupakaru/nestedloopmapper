//============================================================================
// Name        : nested_loop_mapper.cpp
// Author      : Manupa Karunaratne
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "NestedMapping.h"
#include "ComparabilityGraphChecker.h"
#include "OPPSolver.h"
using namespace std;

void insertLoopISODim(std::string loopName,
		              NestedMapping NMpIns,
					  std::map<std::string,std::map<std::string,std::pair<int,int>>>* loopsForCGRAPtr,
		              int XDim,
					  int YDim){

	std::map<std::string,std::set<std::string>> Loop2MUnitMap = NMpIns.getLoop2MUnitMap();
	assert(Loop2MUnitMap.find(loopName)!=Loop2MUnitMap.end());

	for(std::string munit : Loop2MUnitMap[loopName]){
		(*loopsForCGRAPtr)[loopName][munit]=std::make_pair(XDim,YDim);
	}
}



int main(int argc, char* argv[]) {
	NestedMapping NMpIns;
//	NMpIns.parseInput("/home/manupa/manycore/nested_loop_mapper/input/fix_fft.input");

//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/disparity.input4");
//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/svd.input4");
//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/dct.input4");
//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/fix_fft.input4");
//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/aes.input4");
//	NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/proceso.input4");

//	NMpIns.setStrgy(CGRA_CYCLES);

	if(argc == 2){
		NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/fix_fft.input4");
		int cmemSize = atoi(argv[1]);
		std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=GAIN_DENSITY\n";
		std::cin.get();
		NMpIns.findBestLoops(4,4,cmemSize);
		return 0;
	}
	else if(argc == 3){
		NMpIns.parseInputType4("/home/manupa/manycore/nested_loop_mapper/input/fix_fft.input4");
		int cmemSize = atoi(argv[1]);
		std::string strategy(argv[2]);
		if(strategy.compare("CYC")==0){
			NMpIns.setStrgy(CGRA_CYCLES);
			std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=CGRA_CYCLES\n";
			std::cin.get();
			NMpIns.findBestLoops(4,4,cmemSize);
			return 0;
		}
		else{
			assert(strategy.compare("DEN")==0);
			NMpIns.setStrgy(GAIN_DENSITY);
			std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=GAIN_DENSITY\n";
			std::cin.get();
			NMpIns.findBestLoops(4,4,cmemSize);
			return 0;
		}
	}
	else if(argc == 4){
		NMpIns.parseInputType4(argv[3]);
		int cmemSize = atoi(argv[1]);
		std::string strategy(argv[2]);
		if(strategy.compare("CYC")==0){
			NMpIns.setStrgy(CGRA_CYCLES);
			std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=CGRA_CYCLES\n";
			std::cin.get();
			NMpIns.findBestLoops(4,4,cmemSize);
			return 0;
		}
		else if(strategy.compare("MIX")==0){
			NMpIns.setStrgy(MIXED);
			std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=MIXED\n";
			std::cin.get();
			NMpIns.findBestLoops(4,4,cmemSize);
			return 0;
		}
		else{
			assert(strategy.compare("DEN")==0);
			NMpIns.setStrgy(GAIN_DENSITY);
			std::cout << "Press Enter to start with CMEMSize=" << cmemSize << ",STR=GAIN_DENSITY\n";
			std::cin.get();
			NMpIns.findBestLoops(4,4,cmemSize);
			return 0;
		}
	}
//	else{
//		NMpIns.findBestLoops(4,4,32);
//	}

//	return 0;


	std::map<std::string,std::map<std::string,std::pair<int,int>>> loopsForCGRA;

//	insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN121",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN21",NMpIns,&loopsForCGRA,4,4);

//	insertLoopISODim("LN12",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN2",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);

	//proceso

//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN12",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN13",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN14",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN15",NMpIns,&loopsForCGRA,4,4);
//    insertLoopISODim("LN16",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN17",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN18",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN19",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN1a",NMpIns,&loopsForCGRA,4,4);

	//aes
//	insertLoopISODim("LN211",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN231",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN22",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN31",NMpIns,&loopsForCGRA,4,4);
//
//	insertLoopISODim("LN21",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN23",NMpIns,&loopsForCGRA,4,4);
////
//	insertLoopISODim("LN3",NMpIns,&loopsForCGRA,4,4);
////	insertLoopISODim("LN2",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);

	//dct
//	insertLoopISODim("LN111",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN211",NMpIns,&loopsForCGRA,4,4);
//
//	insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN21",NMpIns,&loopsForCGRA,4,4);

//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN2",NMpIns,&loopsForCGRA,4,4);

	//svd
//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN111",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN112",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN113",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN1131",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN1132",NMpIns,&loopsForCGRA,4,4);
//
//	insertLoopISODim("LN2",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN21",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN22",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN23",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN231",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN232",NMpIns,&loopsForCGRA,4,4);
//
//	insertLoopISODim("LN3",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN31",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN32",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN321",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN322",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN33",NMpIns,&loopsForCGRA,4,4);
//
//	insertLoopISODim("LN4",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN41",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN42",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN43",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN431",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN432",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN44",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN45",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN46",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN47",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN48",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN481",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN482",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN49",NMpIns,&loopsForCGRA,4,4);

	//disparity
//	insertLoopISODim("LN1",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN11",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN111",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN12",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN121",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN13",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN14",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN141",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN15",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN151",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN16",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN161",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN17",NMpIns,&loopsForCGRA,4,4);
//	insertLoopISODim("LN171",NMpIns,&loopsForCGRA,4,4);

	ResultNMp res = NMpIns.calcCycles(loopsForCGRA);

	std::cout << "Printing Result : \n";
	std::cout << "CommCycles = " << res.commCycles << "\n";
	std::cout << "CGRACycles = " << res.CGRACycles << "\n";
	std::cout << "HostCycles = " << res.HostCycles << "\n";
	std::cout << "configContexts = " << res.configContexts << "\n";
	std::cout << "Savings=" << res.HostCycles-res.CGRACycles-res.commCycles << "\n";
	return 0;

	ComparabilityGraphChecker CGCIns;

	CGCIns.addNode(1,3);
	CGCIns.addNode(2,3);
	CGCIns.addNode(3,5);
	CGCIns.addNode(4,4);
	CGCIns.addNode(5,6);

	CGCIns.addUndirectedEdge(1,2);
	CGCIns.addUndirectedEdge(2,3);
	CGCIns.addUndirectedEdge(3,4);
	CGCIns.addUndirectedEdge(4,5);
	CGCIns.addUndirectedEdge(5,1);

	//Adding a 2-chord
	CGCIns.addUndirectedEdge(1,3);

	std::set<std::pair<int,int>> c4 = CGCIns.getC4Chords();
	if(!c4.empty()){
		std::cout << "We have a c4 : \n";
		for(std::pair<int,int> nn : c4){
			std::cout << "(" << nn.first << ","  << nn.second << ")," ;
 		}
		std::cout << "\n";
	}

	std::set<std::pair<int,int>> cycle = CGCIns.get2ChordLessCycle();
	if(cycle.empty()){
		std::cout << "There is no cycle\n";

		std::set<node,node_compare> maxClq = CGCIns.getMaxWeightedClique();

		std::cout << "Max weighted clique is : \n";
		for(node n : maxClq){
			std::cout << "node=" << n.idx << ",w=" << CGCIns.W1[n] << "\n";
		}
	}
	else{
		std::cout << "Cycle with Edges : \n";
		for(std::pair<int,int> edge : cycle){
			std::cout << "(" << edge.first << "," << edge.second << "),";
		}
		std::cout << "\n";
	}

	//Oppsolver
	std::set<node,node_compare> V;
	std::map<node,int,node_compare> Wx;
	std::map<node,int,node_compare> Wy;
	std::map<node,int,node_compare> Wz;

	node n1  = node(1);
	node n2  = node(2);
	node n3  = node(3);
//	node n4  = node(4);
//	node n5  = node(5);
//	node n6  = node(6);
//	node n7  = node(7);
//	node n8  = node(8);
//	node n9  = node(9);
//	node n10 = node(10);

	V.insert(n1);
	Wx[n1]=4;Wy[n1]=1;Wz[n1]=15;

	V.insert(n2);
	Wx[n2]=4;Wy[n2]=3;Wz[n2]=6;

	V.insert(n3);
	Wx[n3]=3;Wy[n3]=3;Wz[n3]=2;

//	V.insert(n4);
//	Wx[n4]=3;Wy[n4]=2;Wz[n4]=3;
//
//	V.insert(n5);
//	Wx[n5]=2;Wy[n5]=2;Wz[n5]=4;
//
//	V.insert(n6);
//	Wx[n6]=4;Wy[n6]=2;Wz[n6]=6;
//
//	V.insert(n7);
//	Wx[n7]=3;Wy[n7]=2;Wz[n7]=6;
//
//	V.insert(n8);
//	Wx[n8]=3;Wy[n8]=3;Wz[n8]=5;
//
//	V.insert(n9);
//	Wx[n9]=3;Wy[n9]=3;Wz[n9]=5;
//
//	V.insert(n10);
//	Wx[n10]=2;Wy[n10]=2;Wz[n10]=5;

	OPPSolver oppsolverIns(V,Wx,Wy,Wz);
	oppsolverIns.setLimits(4,4,8);
	if(oppsolverIns.solveOPP()){
		std::cout << "Can fit in and the edges : \n";
		oppsolverIns.printEdges();
	}
	else{
		std::cout << "Cannot fit in\n";
	}



	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
