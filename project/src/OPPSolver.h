/*
 * OPPSolver.h
 *
 *  Created on: 23 Oct 2017
 *      Author: manupa
 */

#ifndef OPPSOLVER_H_
#define OPPSOLVER_H_

#include "NestedMapping.h"
#include <stack>
#include "ComparabilityGraphChecker.h"

enum DIM{X,Y,Z};

struct Aug{
	udEdge edge;
	bool isPlus;
	DIM dim;
	bool init;
	Aug(udEdge edge,bool isPlus,DIM dim) : edge(edge), isPlus(isPlus), dim(dim), init(false){};
	Aug(bool init) : init(init), edge(udEdge(node(-1),node(-2))){};
};

struct searchInfo{
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus_bar;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus_bar;
	Aug aug;

	searchInfo(std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus,
			   std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus_bar,
			   std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus,
			   std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus_bar,
			   Aug aug
			   ):E_plus(E_plus),E_plus_bar(E_plus_bar),E_minus(E_minus),E_minus_bar(E_minus_bar),aug(aug){}
};

class OPPSolver {
public:
	OPPSolver(std::set<node,node_compare> V,
			  std::map<node,int,node_compare> Wx,
			  std::map<node,int,node_compare> Wy,
			  std::map<node,int,node_compare> Wz);
	OPPSolver(){};

	bool checkP3(Aug augIns, std::queue<Aug>* L);
	bool avoidC4(Aug currAug, std::queue<Aug>* L);
	bool avoidClique(Aug currAug);

	void insertEPlus(DIM dim, udEdge e);
	void insertEMinus(DIM dim, udEdge e);
	void insertN(node N, int xN, int yN, int zN);

	searchInfo getCurrSearchInfo(Aug aug);
	void setCurrSearchInfo(searchInfo si);
	void initSearchInfo();

	void setLimits(int xL, int yL, int zL){limits[X]=xL;limits[Y]=yL;limits[Z]=zL;}
	std::queue<Aug> initAug();
	bool updateSearchInfo(Aug currAug);
	std::string packingClassTest(Aug* currAug);
	bool solveOPP();
	bool solveOPP_V2(std::set<node,node_compare> V_new,
			  std::map<node,int,node_compare> Wx_new,
			  std::map<node,int,node_compare> Wy_new,
			  std::map<node,int,node_compare> Wz_new);

	void printDIM(DIM d);
	int printEdges();
	void printE(udEdge e){std::cout << "(" << e.n1.idx << "," << e.n2.idx << ")\n";}
	void validateEdges();

	void clearSearchInfoQ();
	bool isSearchInfoQEmpty(){return searchInfoQ.empty();}
	int getVSize(){return V.size();}

private:
	std::set<node,node_compare> V;

	//Ws for three dimensions
	std::map<DIM,std::map<node,int,node_compare>> W;
	std::map<DIM,int> limits;

	std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_plus_bar;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus;
	std::map<DIM,std::set<udEdge,udEdge_compare>> E_minus_bar;

	std::stack<searchInfo> searchInfoQ;

	void convertC4(std::set<std::pair<int,int>> c4Chords,
			       std::set<udEdge,udEdge_compare>* ude_cycle,
				   std::set<udEdge,udEdge_compare>* ude_chords);



};

#endif /* OPPSOLVER_H_ */
