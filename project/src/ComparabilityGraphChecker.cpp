/*
 * ComparabilityGraphChecker.cpp
 *
 *  Created on: 22 Oct 2017
 *      Author: manupa
 */

#include "ComparabilityGraphChecker.h"
#include <assert.h>
#include <cstdlib>
#include <climits>

ComparabilityGraphChecker::ComparabilityGraphChecker() {
	// TODO Auto-generated constructor stub

}

void ComparabilityGraphChecker::initClassMap() {
	for(std::pair<node,std::set<node,node_compare>> pair1 : E){
		node n1 = pair1.first;
		for(node n2 : pair1.second){
			classMap[n1][n2]=UNDEFINED;
		}
	}

	//zeroing the non-existent edges
	for(node n1 : V){
		if(classMap.find(n1)==classMap.end()){
			for(node n2 : V){
				classMap[n1][n2]=0;
			}
		}
		else{
			for(node n2 : V){
				if(classMap[n1].find(n2)==classMap[n1].end()){
					classMap[n1][n2]=0;
				}
			}
		}
	}

}

void ComparabilityGraphChecker::addUndirectedEdge(int idx1, int idx2) {
	node n1 = getNode(idx1);
	node n2 = getNode(idx2);

	E[n1].insert(n2);
	E[n2].insert(n1);
}

void ComparabilityGraphChecker::addUndirectedEdge(node n1, node n2) {
	E[n1].insert(n2);
	E[n2].insert(n1);

//	V.insert(n1);
//	V.insert(n2);

	assert(V.find(n1)!=V.end());
	assert(V.find(n2)!=V.end());
}

void ComparabilityGraphChecker::addUndirectedEdge(udEdge e) {
	node n1 = e.n1;
	node n2 = e.n2;

	if(V.find(n1)==V.end()){
		std::cout << "n1 missing : " << n1.idx << "\n";
		std::cout << "V.size = " << V.size() << "\n";
	}
	if(V.find(n2)==V.end()){
		std::cout << "n2 missing : " << n2.idx << "\n";
		std::cout << "V.size = " << V.size() << "\n";
	}

	assert(V.find(n1)!=V.end());
	assert(V.find(n2)!=V.end());

	E[n1].insert(n2);
	E[n2].insert(n1);
}

int ComparabilityGraphChecker::CLASS(node n1, node n2) {
	if(E[n1].find(n2)==E[n1].end()){
		assert(classMap[n1].find(n2)==classMap[n1].end());
		return 0;
	}
	return classMap[n1][n2];
}

bool ComparabilityGraphChecker::EXPLORE(node n1, node n2, int k) {
	bool res;
	for (node m : E[n1]){
		if(E[n2].find(m)==E[n2].end()
	       || std::abs(CLASS(n2,m)) < k){     //since undefined is a large number the second check will never be true

			if(CLASS(n1,m)==UNDEFINED){
				classMap[n1][m]=k;
				classMap[m][n1]=-k;
				res = EXPLORE(n1,m,k);
				if(!res) return false;
			}
			else{
				if(CLASS(n1,m)==-k){
					// not a comparability graph return,
					// this implication class contains 2-chordless cycle of odd length
					classMap[n1][m]=k;
					return false;
				}
			}
		}
	}

	for (node m : E[n2]){
		if(E[n1].find(m)==E[n1].end()
	       || std::abs(CLASS(n1,m)) < k){     //since undefined is a large number the second check will never be true
			if(CLASS(m,n2)==UNDEFINED){
				classMap[m][n2]=k;
				classMap[n2][m]=-k;
				res = EXPLORE(m,n2,k);
				if(!res) return false;
			}
			else{
				if(CLASS(m,n2)==-k){
					// not a comparability graph return,
					// this implication class contains 2-chordless cycle of odd length
					classMap[m][n2]=k;
					return false;
				}
			}
		}
	}

	return true;
}

std::set<std::pair<int,int>> ComparabilityGraphChecker::get2ChordLessCycle() {
	int k = 0;
	initClassMap();
	bool res;
	std::set<std::pair<int,int>> twochordlessCycle;
	for (std::pair<node,std::set<node,node_compare>> pair1 : E){
		node n1 = pair1.first;
		for (node n2 : pair1.second){
			if(classMap[n1][n2]==UNDEFINED){
				k=k+1;
				classMap[n1][n2]=k;
				classMap[n2][n1]=-k;
				res = EXPLORE(n1,n2,k);

				if(!res){
					for(std::pair<node,std::map<node,int,node_compare>> pair2 : classMap){
						node m1 = pair2.first;
						for(std::pair<node,int> pair3 : pair2.second){
							node m2 = pair3.first;
							if(pair3.second == k){
								twochordlessCycle.insert(std::make_pair(m1.idx,m2.idx));
							}
						}
					}
					return twochordlessCycle;
				}
			}
		}
	}
	isComparibilityGraph=true;
	return twochordlessCycle;
}

void ComparabilityGraphChecker::setE(std::set<udEdge, udEdge_compare> UdE) {
	for(udEdge e : UdE){
		E[e.n1].insert(e.n2);
		E[e.n2].insert(e.n1);

		assert(V.find(e.n1)!=V.end());
		assert(V.find(e.n2)!=V.end());

	}
}



void ComparabilityGraphChecker::DFS(node n1,
		                            std::set<node,node_compare>* exploredNodes,
									std::map<node,int,node_compare>* accW,
									std::map<node,node,node_compare>* goingTo,
		                            std::map<node, std::set<node, node_compare>, node_compare> adj) {


	//std::cout << "DFS : node =" << n1.idx << ",weight=" << W1[n1] << "\n";
	//std::cout << "adj.size()=" << adj.size() << "\n";

	exploredNodes->insert(n1);

	if(adj.find(n1)==adj.end()){
		(*accW)[n1]=W1[n1];
		return;
	}

	if(adj[n1].empty()){
		(*accW)[n1]=W1[n1];
		return;
	}

	//std::cout << "Doing further DFS...\n";

	for (node x : adj[n1]){
		if(exploredNodes->find(x)==exploredNodes->end()){
			DFS(x,exploredNodes,accW,goingTo,adj);
		}
	}

	int maxWeight=0;
	node maxNodeY;
	for (node x : adj[n1]){
		if((*accW)[x] > maxWeight){
			maxWeight = (*accW)[x];
			maxNodeY = x;
		}
	}

	(*accW)[n1] = W1[n1] + maxWeight;
	(*goingTo)[n1] = maxNodeY;
}

std::map<node, std::set<node, node_compare>, node_compare> ComparabilityGraphChecker::getTransOri() {
	std::map<node, std::set<node, node_compare>, node_compare> transOri;

	assert(isComparibilityGraph);
	std::set<node,node_compare> checknodeset;

	for (std::pair<node,std::map<node,int,node_compare>> pair1 : classMap){
		node n1 = pair1.first;
		for (std::pair<node,int> pair2 : pair1.second){
			node n2 = pair2.first;
			if(pair2.second > 0){
				transOri[n1].insert(n2);

				checknodeset.insert(n1);
				checknodeset.insert(n2);
			}
		}
	}
//	if(checknodeset.size()!=V.size()){
		//std::cout << "checknodeset.size()=" << checknodeset.size() << "\n";
		//std::cout << "transOri.size()=" << transOri.size() << "\n";
//	}
//	assert(checknodeset.size()==V.size());
	return transOri;
}

void ComparabilityGraphChecker::addNode(int idx, int weight) {
	node n(idx);
	V.insert(n);
	W1[n]=weight;
}

node ComparabilityGraphChecker::getNode(int idx) {
	for(node n : V){
		if(n.idx == idx){
			return n;
		}
	}
	assert(false);
	return node(-1);
}

std::set<node, node_compare> ComparabilityGraphChecker::getMaxWeightedClique() {
	std::map<node, std::set<node, node_compare>, node_compare> transOri = getTransOri();
	std::set<node,node_compare> exploredNodes;
	std::map<node,int,node_compare> accW;
	std::map<node,node,node_compare> goingTo;

	std::set<node, node_compare> maxClique;

	for (node n1 : V){
		if(exploredNodes.find(n1)==exploredNodes.end()){
			DFS(n1,&exploredNodes,&accW,&goingTo,transOri);
		}
	}

	int maxWeight=0;
	node y;
	for (node x : V){
		if(accW[x] > maxWeight){
			//std::cout << "accW[x] = " << accW[x] << "\n";
			maxWeight = accW[x];
			y =x;
		}
	}

	maxClique.insert(y);
//	assert(goingTo.find(y)!=goingTo.end());
//	y = goingTo[y];
	while(goingTo.find(y)!=goingTo.end()){
		y = goingTo[y];
		maxClique.insert(y);
	}
//	maxClique.insert(y);

	return maxClique;
}

std::set<std::pair<int,int>> ComparabilityGraphChecker::getC4Chords() {

	std::map<std::pair<int,int>,int> edgeC4;
	std::set<std::pair<int,int>> result;

	for(node n1 : V){
		for(node n1_a1 : E[n1]){
			for(node n1_a2 : E[n1]){
				if(n1_a2.idx==n1_a1.idx)continue;

				//if two neighbours of n1 are not adjacent to each other
				if(E[n1_a1].find(n1_a2)==E[n1_a1].end()){

					std::pair<int,int> edge  = std::make_pair(n1_a1.idx,n1_a2.idx);
					std::pair<int,int> edge_ = std::make_pair(n1_a2.idx,n1_a1.idx);

////					std::cout << "n1=" << n1.idx << ",";
////					std::cout << "edge.first=" << edge.first << ",";
////					std::cout << "edge.second=" << edge.second << "\n";

					if(edgeC4.find(edge)!=edgeC4.end()){
						if(edgeC4[edge]!=n1.idx){
							if(E[n1].find(edgeC4[edge])==E[n1].end()){
								result.insert(edge);
								result.insert(std::make_pair(n1.idx,edgeC4[edge]));
								return result;
							}
						}
					}
					edgeC4[edge] =n1.idx;
					edgeC4[edge_]=n1.idx;
				}
			}
		}
	}

	return result;
}

void ComparabilityGraphChecker::reset() {
	V.clear();
	E.clear();
	classMap.clear();
	isComparibilityGraph=false;
}
