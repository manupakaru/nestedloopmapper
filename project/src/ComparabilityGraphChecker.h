/*
 * ComparabilityGraphChecker.h
 *
 *  Created on: 22 Oct 2017
 *      Author: manupa
 */

#ifndef COMPARABILITYGRAPHCHECKER_H_
#define COMPARABILITYGRAPHCHECKER_H_

#include "NestedMapping.h"

class ComparabilityGraphChecker {
public:
	ComparabilityGraphChecker();
	void initClassMap();
	void addUndirectedEdge(node n1, node n2);
	void addUndirectedEdge(int idx1, int idx2);
	void addUndirectedEdge(udEdge e);
	void addNode(int idx, int weight);
	node getNode(int idx);

	std::set<std::pair<int,int>> get2ChordLessCycle();

	//max weighted clique
	std::map<node,std::set<node,node_compare>,node_compare> getTransOri();
	std::set<node,node_compare> getMaxWeightedClique();
	std::set<std::pair<int,int>> getC4Chords();
	void reset();

	void setV(std::set<node,node_compare> V_, std::map<node,int,node_compare> W_){V=V_;W1=W_;}
	std::set<node,node_compare>* getVPtr(){return &V;};

	void setE(std::set<udEdge,udEdge_compare> UdE);
	void setE(std::map<node,std::set<node,node_compare>,node_compare> E_){E=E_;}
	std::map<node,std::set<node,node_compare>,node_compare>* getEPtr(){return &E;}

	//public variables
	std::map<node,int,node_compare> W1;

private:
	std::set<node,node_compare> V;
	std::map<node,std::set<node,node_compare>,node_compare> E;
	std::map<node,std::map<node,int,node_compare>,node_compare> classMap;
	bool isComparibilityGraph=false;

	int  CLASS(node n1, node n2);
	bool EXPLORE(node n1, node n2, int k);
	void DFS(node n1,
             std::set<node,node_compare>* exploredNodes,
			 std::map<node,int,node_compare>* accW,
			 std::map<node,node,node_compare>* goingTo,
             std::map<node, std::set<node, node_compare>, node_compare> adj);



};

#endif /* COMPARABILITYGRAPHCHECKER_H_ */
