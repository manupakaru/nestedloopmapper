/*
 * NestedMapping.cpp
 *
 *  Created on: 20 Oct 2017
 *      Author: manupa
 */

#include "NestedMapping.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <assert.h>
#include <stdlib.h>
#include <queue>
#include <stack>
#include <functional>     // std::greater
#include "OPPSolver.h"

//store interim results
struct interimRes{
	int savings;
	bool isValid=false;
	ResultNMp cycleCounts;
	OPPSolver oppS;
};

struct searchInfoKS{
	std::string latestLplus = "NA";
	std::map<std::string,boxDims> mUnitDims;
	std::set<std::string> L_plus;
	std::set<std::string> L_potential;
	std::set<std::string> L_minus;
	interimRes res;

	std::vector<int> munitComboIdxVec;
};



NestedMapping::NestedMapping() {
	// TODO Auto-generated constructor stub

}

int NestedMapping::parseInput(std::string fileName) {

	std::ifstream inputFile;
	inputFile.open(fileName.c_str());

	if(!inputFile.is_open()){
		std::cout << "input File : " << fileName << "opening failure.\n";
		return -1;
	}

	std::string line;
	while(std::getline(inputFile,line)){
		if(!line.empty()){
			std::stringstream ss(line);
			std::vector<std::string> tokens;
			std::string singleToken;

			while(std::getline(ss,singleToken,',')){
				tokens.push_back(singleToken);
			}

			if(tokens[0].compare("FCALLS")==0){
				assert(tokens.size()==2);
				fCalls=atoi(tokens[1].c_str());
			}
			else if (tokens[0].compare("BB_INFO")==0){
				while(std::getline(inputFile,line)){
					if(line.empty())continue;
					if(line.compare("ENDSECTION")==0)break;
					std::stringstream ss(line);
					std::vector<std::string> tokens;
					while(std::getline(ss,singleToken,',')){
						tokens.push_back(singleToken);
					}
					assert(tokens.size()==4);

					std::string BBName = tokens[0];
					int NodeCount = atoi(tokens[1].c_str());
					std::string loopName = tokens[2];
					std::string munitName = tokens[3];

					BBNodeCountMap[BBName]=NodeCount;
					MUnit2BBMap[munitName].insert(BBName);
					Loop2MUnitMap[loopName].insert(munitName);
				}
			}
			else if (tokens[0].compare("BB_INVO")==0){
				while(std::getline(inputFile,line)){
					if(line.empty())continue;
					if(line.compare("ENDSECTION")==0)break;
					std::stringstream ss(line);
					std::vector<std::string> tokens;
					while(std::getline(ss,singleToken,',')){
						tokens.push_back(singleToken);
					}
					assert(tokens.size()==4);

					std::string BBName = tokens[0];
					int InvCount = atoi(tokens[1].c_str());
					std::string loopName = tokens[2];
					std::string munitName = tokens[3];

					if(BBNodeCountMap.find(BBName)==BBNodeCountMap.end()){
						continue;
					}
					BBInvoMap[BBName]=InvCount;
				}
			}
			else if (tokens[0].compare("INVO_WEIGHTED_DATA_SCALAR_TRANSFER_EDGES")==0){
				while(std::getline(inputFile,line)){
					if(line.empty())continue;
					if(line.compare("ENDSECTION")==0)break;
					std::stringstream ss(line);
					std::vector<std::string> tokens;
					while(std::getline(ss,singleToken,',')){
						tokens.push_back(singleToken);
					}
					assert(tokens.size()==3);

					std::string SrcLp = tokens[0];
					std::string DestLp = tokens[1];
					int InvWeightedScalarDataCount = atoi(tokens[2].c_str());
					weightedScalarTransEdges[SrcLp][DestLp]=InvWeightedScalarDataCount;
				}
			}
			else if (tokens[0].compare("INVO_WEIGHTED_DATA_ARRAY_TRANSFER_EDGES")==0){
				while(std::getline(inputFile,line)){
					if(line.empty())continue;
					if(line.compare("ENDSECTION")==0)break;
					std::stringstream ss(line);
					std::vector<std::string> tokens;
					while(std::getline(ss,singleToken,',')){
						tokens.push_back(singleToken);
					}
					assert(tokens.size()==3);

					std::string SrcLp = tokens[0];
					std::string DestLp = tokens[1];
					int InvWeightedScalarDataCount = atoi(tokens[2].c_str());
					weightedArrTransEdges[SrcLp][DestLp]=InvWeightedScalarDataCount;
				}
			}
			else if (tokens[0].compare("MAPPABLE_BLOCKS")==0){
				while(std::getline(inputFile,line)){
					if(line.empty())continue;
					if(line.compare("ENDSECTION")==0)break;
					std::stringstream ss(line);
					std::vector<std::string> tokens;
					while(std::getline(ss,singleToken,',')){
						tokens.push_back(singleToken);
					}
					assert(tokens.size()==4);

					std::string munitName = tokens[0];
					int XDim = atoi(tokens[1].c_str());
					int YDim = atoi(tokens[2].c_str());
					int II   = atoi(tokens[3].c_str());
					MappableBlocks[munitName][YDim][XDim]=II;
				}
			}
			else{
				std::cout << "Unrecognized : " << tokens[0] << "\n";
				assert(false);
			}

		}
	}

	populateLoopConstraints();
	return 0;
}

int NestedMapping::parseInputType2(std::string fileName) {

	std::ifstream inputFile;
		inputFile.open(fileName.c_str());

		if(!inputFile.is_open()){
			std::cout << "input File : " << fileName << "opening failure.\n";
			return -1;
		}

		std::string line;
		while(std::getline(inputFile,line)){
			if(!line.empty()){
				std::stringstream ss(line);
				std::vector<std::string> tokens;
				std::string singleToken;

				while(std::getline(ss,singleToken,',')){
					tokens.push_back(singleToken);
				}

				if(tokens[0].compare("FCALLS")==0){
					assert(tokens.size()==2);
					fCalls=atoi(tokens[1].c_str());
				}
				else if (tokens[0].compare("BB_INFO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int NodeCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						BBNodeCountMap[BBName]=NodeCount;
						MUnit2BBMap[munitName].insert(BBName);
						Loop2MUnitMap[loopName].insert(munitName);
//						std::cout << "munitName=" << munitName << ",loopName=" <<  loopName << "\n";
						Munit2LoopMap[munitName]=loopName;
					}
				}
				else if (tokens[0].compare("BB_INVO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int InvCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						if(BBNodeCountMap.find(BBName)==BBNodeCountMap.end()){
							continue;
						}
						BBInvoMap[BBName]=InvCount;
					}
				}
				else if (tokens[0].compare("INVOCATION_MUNITS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==3);

						std::string SrcMUnit = tokens[0];
						std::string DestMunit = tokens[1];
						int Invo = atoi(tokens[2].c_str());
						Munit2MunitInvo[SrcMUnit][DestMunit]=Invo/fCalls;
					}
				}
				else if (tokens[0].compare("DATA_SCALAR_TRANSFER_EDGES")==0){
					std::cout << "Scalar::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

//						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>3);

						std::string SrcMUnit = tokens[0];
						std::string DestMunit = tokens[1];
						int size = atoi(tokens[2].c_str())*4;

						int offsetIdx = atoi(tokens[2].c_str())+1;
						int min = 1000000;

//						std::cout << "tokens::";
//						for(std::string token : tokens){
//							std::cout << token << ",";
//						}
//						std::cout << "\n";

						std::string currMunit=SrcMUnit;
//						std::cout << "minCalc::";
						for (unsigned int j = 2+offsetIdx; j < tokens.size(); j++) {
							std::string nextMunit=tokens[j];
//							std::cout << "nextMunit=" << nextMunit << ",";
							if(Munit2MunitInvo.find(currMunit)==Munit2MunitInvo.end()){
								min=0;
//								std::cout << nextMunit << "=0\n";
								break;
							}

							if(Munit2MunitInvo[currMunit].find(nextMunit)==Munit2MunitInvo[currMunit].end()){
								min=0;
//								std::cout << nextMunit << "=0\n";
								break;
							}

							if(Munit2MunitInvo[currMunit][nextMunit] < min){
								min = Munit2MunitInvo[currMunit][nextMunit];
//								std::cout << nextMunit << "=" << min << "\n";
							}
							currMunit = nextMunit;
						}

//						std::cout << "SrcMunit=" << SrcMUnit << ",DestMunit=" << DestMunit << ",sizeInc =" << min*size << "\n";

						assert(DestMunit.compare(tokens.back())==0);

						std::string SrcLp = "FUNC_BODY";
						if(Munit2LoopMap.find(currMunit)!=Munit2LoopMap.end()){
							SrcLp=Munit2LoopMap[SrcMUnit];
						}

						std::string DestLp = "FUNC_BODY";
						if(Munit2LoopMap.find(DestMunit)!=Munit2LoopMap.end()){
							DestLp=Munit2LoopMap[DestMunit];
						}

						if(SrcLp.compare(DestLp)!=0 && min*size > 0){
							std::cout << "SrcLp=" << SrcLp << ",DestLp=" << DestLp << ",+size=" <<  min*size << "\n";
						}

						if(Loop2LoopInvoWeightedScalar.find(SrcLp)==Loop2LoopInvoWeightedScalar.end()){
							Loop2LoopInvoWeightedScalar[SrcLp][DestLp]=min*size;
						}
						else{
							if(Loop2LoopInvoWeightedScalar[SrcLp].find(DestLp)==Loop2LoopInvoWeightedScalar[SrcLp].end()){
								Loop2LoopInvoWeightedScalar[SrcLp][DestLp]=min*size;
							}
							else{
								Loop2LoopInvoWeightedScalar[SrcLp][DestLp]+=min*size;
							}
						}


					}
				}
				else if (tokens[0].compare("DATA_ARRAY_SIZES")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==2);

						std::string arrName = tokens[0];
						int arrSize = atoi(tokens[1].c_str());
						arrSizes[arrName]=arrSize;
					}
				}
				else if (tokens[0].compare("DATA_ARRAY_TRANSFER_EDGES")==0){
					std::cout << "Array::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>3);

						std::string SrcMUnit = tokens[0];
						std::string DestMunit = tokens[1];
						int numberofArrs = atoi(tokens[2].c_str());

						int size=0;
						for (int i = 3; i < 3+numberofArrs; ++i) {
//							std::cout << "arrName=" << tokens[i] << "\n";
							assert(arrSizes.find(tokens[i])!=arrSizes.end());
							size+=arrSizes[tokens[i]];
						}

						int offsetIdx = atoi(tokens[2].c_str())+1;
						int min = 1000000;

						std::string currMunit=SrcMUnit;
//						std::cout << "minCalc::";
						for (int i = 2+offsetIdx; i < tokens.size(); ++i) {
							std::string nextMunit=tokens[i];
//							std::cout << "nextMunit=" << nextMunit;
							if(Munit2MunitInvo.find(currMunit)==Munit2MunitInvo.end()){
								min=0;
//								std::cout << "=0\n";
								break;
							}

							if(Munit2MunitInvo[currMunit].find(nextMunit)==Munit2MunitInvo[currMunit].end()){
								min=0;
//								std::cout << "=0\n";
								break;
							}

							if(Munit2MunitInvo[currMunit][nextMunit] < min){
								min = Munit2MunitInvo[currMunit][nextMunit];
							}
//							std::cout << "=(" << min << "," << Munit2MunitInvo[currMunit][nextMunit] << "),";
							currMunit = nextMunit;
						}

						assert(DestMunit.compare(tokens.back())==0);

//						std::cout << "SrcMunit=" << SrcMUnit << ",DestMunit=" << DestMunit << ",sizeInc =" << min*size << "\n";

						std::string SrcLp = "FUNC_BODY";
						if(Munit2LoopMap.find(SrcMUnit)!=Munit2LoopMap.end()){
							SrcLp=Munit2LoopMap[SrcMUnit];
						}
						std::string DestLp = "FUNC_BODY";
						if(Munit2LoopMap.find(DestMunit)!=Munit2LoopMap.end()){
							DestLp=Munit2LoopMap[DestMunit];
						}

						int dataSize = min*size;

//						if(SrcLp.compare(DestLp)!=0 && dataSize > 0){
//							std::cout << "SrcLp=" << SrcLp << ",DestLp=" << DestLp << ",+size=" <<  dataSize << "\n";
//						}

						if(Munit2MunitInvoWeightedArr.find(SrcMUnit)==Munit2MunitInvoWeightedArr.end()){
							Munit2MunitInvoWeightedArr[SrcMUnit][DestMunit]=dataSize;
						}
						else{
							if(Munit2MunitInvoWeightedArr[SrcMUnit].find(DestMunit)==Munit2MunitInvoWeightedArr[SrcMUnit].end()){
								Munit2MunitInvoWeightedArr[SrcMUnit][DestMunit]=dataSize;
							}
							else{
								Munit2MunitInvoWeightedArr[SrcMUnit][DestMunit]=std::max(dataSize,Munit2MunitInvoWeightedArr[SrcMUnit][DestMunit]);
							}
						}

//						if(Loop2LoopInvoWeightedArr.find(SrcLp)==Loop2LoopInvoWeightedArr.end()){
//							Loop2LoopInvoWeightedArr[SrcLp][DestLp]=dataSize;
//						}
//						else{
//							if(Loop2LoopInvoWeightedArr[SrcLp].find(DestLp)==Loop2LoopInvoWeightedArr[SrcLp].end()){
//								Loop2LoopInvoWeightedArr[SrcLp][DestLp]=dataSize;
//							}
//							else{
//								Loop2LoopInvoWeightedArr[SrcLp][DestLp]+=dataSize;
//							}
//						}
					}

					for(std::pair<std::string,std::map<std::string,int>> pair1 : Munit2MunitInvoWeightedArr){
						for(std::pair<std::string,int> pair2 : pair1.second){
							std::string SrcMUnit=pair1.first;
							std::string DestMunit=pair2.first;
							int dataSize = pair2.second;

							std::string SrcLp = "FUNC_BODY";
							if(Munit2LoopMap.find(SrcMUnit)!=Munit2LoopMap.end()){
								SrcLp=Munit2LoopMap[SrcMUnit];
							}
							std::string DestLp = "FUNC_BODY";
							if(Munit2LoopMap.find(DestMunit)!=Munit2LoopMap.end()){
								DestLp=Munit2LoopMap[DestMunit];
							}

							if(SrcLp.compare(DestLp)!=0 && dataSize > 0){
								std::cout << "SrcLp=" << SrcLp << ",DestLp=" << DestLp << ",+size=" <<  dataSize << "\n";
							}

							if(Loop2LoopInvoWeightedArr.find(SrcLp)==Loop2LoopInvoWeightedArr.end()){
								Loop2LoopInvoWeightedArr[SrcLp][DestLp]=dataSize;
							}
							else{
								if(Loop2LoopInvoWeightedArr[SrcLp].find(DestLp)==Loop2LoopInvoWeightedArr[SrcLp].end()){
									Loop2LoopInvoWeightedArr[SrcLp][DestLp]=dataSize;
								}
								else{
									Loop2LoopInvoWeightedArr[SrcLp][DestLp]+=dataSize;
								}
							}

						}
					}

				}
				else if (tokens[0].compare("MAPPABLE_BLOCKS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string munitName = tokens[0];
						int XDim = atoi(tokens[1].c_str());
						int YDim = atoi(tokens[2].c_str());
						int II   = atoi(tokens[3].c_str());
						MappableBlocks[munitName][YDim][XDim]=II;
					}
				}
				else{
					std::cout << "Unrecognized : " << tokens[0] << "\n";
					assert(false);
				}

			}
		}

		populateLoopConstraints();
		return 0;


}

int NestedMapping::parseInputType3(std::string fileName) {

	std::ifstream inputFile;
		inputFile.open(fileName.c_str());

		if(!inputFile.is_open()){
			std::cout << "input File : " << fileName << "opening failure.\n";
			return -1;
		}

		std::string line;
		while(std::getline(inputFile,line)){
			if(!line.empty()){
				std::stringstream ss(line);
				std::vector<std::string> tokens;
				std::string singleToken;

				while(std::getline(ss,singleToken,',')){
					tokens.push_back(singleToken);
				}

				if(tokens[0].compare("FCALLS")==0){
					assert(tokens.size()==2);
					fCalls=atoi(tokens[1].c_str());
				}
				else if (tokens[0].compare("BB_INFO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int NodeCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						BBNodeCountMap[BBName]=NodeCount;
						MUnit2BBMap[munitName].insert(BBName);
						Loop2MUnitMap[loopName].insert(munitName);
//						std::cout << "munitName=" << munitName << ",loopName=" <<  loopName << "\n";
						Munit2LoopMap[munitName]=loopName;
					}
				}
				else if (tokens[0].compare("BB_INVO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int InvCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						if(BBNodeCountMap.find(BBName)==BBNodeCountMap.end()){
							continue;
						}
						BBInvoMap[BBName]=InvCount;
					}
				}
				else if (tokens[0].compare("INVOCATION_LOOPS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==2);

						std::string Lp = tokens[0];
						int Invo = atoi(tokens[1].c_str());
						loopInv[Lp]=Invo;
					}
				}
				else if (tokens[0].compare("INVOCATION_MUNITS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==3);

						std::string SrcMUnit = tokens[0];
						std::string DestMunit = tokens[1];
						int Invo = atoi(tokens[2].c_str());
						Munit2MunitInvo[SrcMUnit][DestMunit]=Invo/fCalls;
					}
				}
				else if (tokens[0].compare("DATA_SCALAR_TRANSFER_EDGES")==0){
					std::cout << "Scalar::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

//						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>5);

						std::string SrcMUnit = tokens[0];
						std::string SrcLp=tokens[1];
						std::string DestMunit = tokens[2];
						std::string DestLp=tokens[3];
						int offsetIdx = 5;

						for (unsigned int j = offsetIdx; j < tokens.size(); j++) {
							scalarVarSetEdges[SrcLp][DestLp].insert(tokens[j]);
						}
					}
				}
				else if (tokens[0].compare("DATA_ARRAY_SIZES")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==2);

						std::string arrName = tokens[0];
						int arrSize = atoi(tokens[1].c_str());
						arrSizes[arrName]=arrSize;
					}
				}
				else if (tokens[0].compare("DATA_ARRAY_TRANSFER_EDGES")==0){
					std::cout << "Array::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

//						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>5);

						std::string SrcMUnit = tokens[0];
						std::string SrcLp=tokens[1];
						std::string DestMunit = tokens[2];
						std::string DestLp=tokens[3];
						int offsetIdx = 5;

						std::stack<std::string> dfs;
						dfs.push(SrcMUnit);
						std::set<std::string> visited;
						bool found=false;
//						std::cout << "Searching from:" << SrcMUnit << ",to:" << DestMunit << "\n";
						while(!dfs.empty()){
							std::string currMunit = dfs.top();
							dfs.pop();
//							std::cout << "DFS : currMunit=" << currMunit << "\n";
							if(currMunit.compare(DestMunit)==0){
								found=true;
								break;
							}
							visited.insert(currMunit);
							if(Munit2MunitInvo.find(currMunit)!=Munit2MunitInvo.end()){
								for(std::pair<std::string,int> pair : Munit2MunitInvo[currMunit]){
									std::string nextMunit = pair.first;
									if(visited.find(nextMunit)==visited.end()){
										dfs.push(nextMunit);
									}
								}
							}
						}

						if(found){
							std::cout << line << "\n";
							for (unsigned int j = offsetIdx; j < tokens.size(); j++) {
								arrVarSetEdges[SrcLp][DestLp].insert(tokens[j]);
							}
						}
					}
				}
				else if (tokens[0].compare("MAPPABLE_BLOCKS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string munitName = tokens[0];
						int XDim = atoi(tokens[1].c_str());
						int YDim = atoi(tokens[2].c_str());
						int II   = atoi(tokens[3].c_str());
						MappableBlocks[munitName][YDim][XDim]=II;
					}
				}
				else{
					std::cout << "Unrecognized : " << tokens[0] << "\n";
					assert(false);
				}

			}
		}

		populateLoopConstraints();
		return 0;


}

int NestedMapping::parseInputType4(std::string fileName) {

	std::ifstream inputFile;
		inputFile.open(fileName.c_str());

		if(!inputFile.is_open()){
			std::cout << "input File : " << fileName << "opening failure.\n";
			return -1;
		}

		std::string line;
		while(std::getline(inputFile,line)){
			if(!line.empty()){
				std::stringstream ss(line);
				std::vector<std::string> tokens;
				std::string singleToken;

				while(std::getline(ss,singleToken,',')){
					tokens.push_back(singleToken);
				}

				if(tokens[0].compare("FCALLS")==0){
					assert(tokens.size()==2);
					fCalls=atoi(tokens[1].c_str());
				}
				else if (tokens[0].compare("BB_INFO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int NodeCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						BBNodeCountMap[BBName]=NodeCount;
						MUnit2BBMap[munitName].insert(BBName);
						Loop2MUnitMap[loopName].insert(munitName);
//						std::cout << "munitName=" << munitName << ",loopName=" <<  loopName << "\n";
						Munit2LoopMap[munitName]=loopName;
					}
				}
				else if (tokens[0].compare("BB_INVO")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==4);

						std::string BBName = tokens[0];
						int InvCount = atoi(tokens[1].c_str());
						std::string loopName = tokens[2];
						std::string munitName = tokens[3];

						if(BBNodeCountMap.find(BBName)==BBNodeCountMap.end()){
							continue;
						}
						BBInvoMap[BBName]=InvCount;
					}
				}
				else if (tokens[0].compare("INVOCATION_LOOPS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==2);

						std::string Lp = tokens[0];
						int Invo = atoi(tokens[1].c_str());
						loopInv[Lp]=Invo;
					}
				}
				else if (tokens[0].compare("INVOCATION_MUNITS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==3);

						std::string SrcMUnit = tokens[0];
						std::string DestMunit = tokens[1];
						int Invo = atoi(tokens[2].c_str());
						Munit2MunitInvo[SrcMUnit][DestMunit]=Invo/fCalls;
					}
					for(std::pair<std::string,std::set<std::string>> toppair1  : Loop2MUnitMap){
						std::string lp = toppair1.first;
						for(std::string munit : toppair1.second){

							int maxBBInv = 0;
							for(std::string BB : MUnit2BBMap[munit]){
								if(BBInvoMap[BB] > maxBBInv){
									maxBBInv=BBInvoMap[BB];
								}
							}
							MUnitIterMap[munit]=maxBBInv;
							std::cout << "IterMunit : " << munit << "," << maxBBInv << "\n";
							int currMunitInvo=0;
							for(std::pair<std::string,std::map<std::string,int>> pair2 : Munit2MunitInvo){
								std::string srcMunit = pair2.first;
								for(std::pair<std::string,int> pair3 : pair2.second){
									std::string destMunit = pair3.first;
									if(destMunit.compare(munit)==0){
										currMunitInvo+=pair3.second;
									}
								}
							}
							MUnitInvoMap[munit]=currMunitInvo;
							std::cout << "InvoMunit : " << munit << "," << currMunitInvo << "\n";
						}
					}
				}
				else if (tokens[0].compare("DATA_SCALAR_TRANSFER_EDGES")==0){
					std::cout << "Scalar::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

//						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>5);

						std::string SrcMUnit = tokens[0];
						std::string SrcLp=tokens[1];
						std::string DestMunit = tokens[2];
						std::string DestLp=tokens[3];
						int offsetIdx = 5;

						for (unsigned int j = offsetIdx; j < tokens.size(); j++) {
							scalarVarSetEdges[SrcLp][DestLp].insert(tokens[j]);
						}
					}
				}
				else if (tokens[0].compare("DATA_ARRAY_SIZES")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==2);

						std::string arrName = tokens[0];
						int arrSize = atoi(tokens[1].c_str());
						arrSizes[arrName]=arrSize;
					}
				}
				else if (tokens[0].compare("DATA_ARRAY_TRANSFER_EDGES")==0){
					std::cout << "Array::\n";
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;

//						std::cout << line << "\n";

						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()>5);

						std::string SrcMUnit = tokens[0];
						std::string SrcLp=tokens[1];
						std::string DestMunit = tokens[2];
						std::string DestLp=tokens[3];
						int offsetIdx = 5;

						std::stack<std::string> dfs;
						dfs.push(SrcMUnit);
						std::set<std::string> visited;
						bool found=false;
//						std::cout << "Searching from:" << SrcMUnit << ",to:" << DestMunit << "\n";
						while(!dfs.empty()){
							std::string currMunit = dfs.top();
							dfs.pop();
//							std::cout << "DFS : currMunit=" << currMunit << "\n";
							if(currMunit.compare(DestMunit)==0){
								found=true;
								break;
							}
							visited.insert(currMunit);
							if(Munit2MunitInvo.find(currMunit)!=Munit2MunitInvo.end()){
								for(std::pair<std::string,int> pair : Munit2MunitInvo[currMunit]){
									std::string nextMunit = pair.first;
									if(visited.find(nextMunit)==visited.end()){
										dfs.push(nextMunit);
									}
								}
							}
						}

						if(found){
							std::cout << line << "\n";
							for (unsigned int j = offsetIdx; j < tokens.size(); j++) {
								arrVarSetEdges[SrcLp][DestLp].insert(tokens[j]);
							}
						}
					}
				}
				else if (tokens[0].compare("MAPPABLE_BLOCKS")==0){
					while(std::getline(inputFile,line)){
						if(line.empty())continue;
						if(line.compare("ENDSECTION")==0)break;
						std::stringstream ss(line);
						std::vector<std::string> tokens;
						while(std::getline(ss,singleToken,',')){
							tokens.push_back(singleToken);
						}
						assert(tokens.size()==5);

						std::string munitName = tokens[0];
						int XDim = atoi(tokens[1].c_str());
						int YDim = atoi(tokens[2].c_str());
						int II   = atoi(tokens[3].c_str());
						int Lat  = atoi(tokens[4].c_str());
						MappableBlocks[munitName][YDim][XDim]=II;
						MappableBlocksLatency[munitName][YDim][XDim]=Lat;
					}
				}
				else{
					std::cout << "Unrecognized : " << tokens[0] << "\n";
					assert(false);
				}

			}
		}

		populateLoopConstraints();
		return 0;


}

ResultNMp NestedMapping::calcCycles(
		std::map<std::string, std::map<std::string, std::pair<int, int> > > loopsForCGRA) {

		ResultNMp res;

		std::vector<std::string> loopsForHost;
		loopsForHost.push_back("FUNC_BODY");
		enum partition {HOST,CGRA};

				 //loopName
		std::map<std::string,partition> loopParition;

		for(std::pair<std::string,std::set<std::string> > pair : Loop2MUnitMap){
			std::string loopName = pair.first;
	//		if(std::find(loopsForCGRA.begin(),loopsForCGRA.end(),loopName)==loopsForCGRA.end()){
			if(loopsForCGRA.find(loopName)==loopsForCGRA.end()){
				loopParition[loopName]=HOST;
			}
			else{
				loopParition[loopName]=CGRA;
			}
		}

		//Calculate Comm Cycles
		float scalarCommCycles=0;
//		std::cout << "Calculating Scalar Edges...\n";
		std::map<std::pair<std::string,std::string>,std::set<std::string>> scalarTransVars;

		for ( std::pair<std::string,std::map<std::string,std::set<std::string>>> pair1 : scalarVarSetEdges){
			std::string srcLp = pair1.first;
			for(std::pair<std::string,std::set<std::string>> pair2 : pair1.second){
				std::string destLp = pair2.first;

				if(loopParition[srcLp]!=loopParition[destLp]){
					for(std::string varName : pair2.second){
						std::string srcLpTrans=srcLp;
						std::string destLpTrans=destLp;

//						std::cout << "srcLp=" << srcLp << ",destLp=" << destLp << "\n";
						if(srcLp.compare("FUNC_BODY")!=0 && destLp.compare("FUNC_BODY")!=0 ){
							if(srcLp.size() > destLp.size()){
								srcLpTrans=srcLp.substr(0,destLp.size());
							}
							else{
								destLpTrans=destLp.substr(0,srcLp.size());
							}
						}
//						std::cout << "srcLpTrans=" << srcLpTrans << ",destLpTrans=" << destLpTrans << "\n";
						scalarTransVars[std::make_pair(srcLpTrans,destLpTrans)].insert(varName);
					}
				}
			}
		}

//		std::cout << "calculating inv..\n";

		for(std::pair<std::pair<std::string,std::string>,std::set<std::string>> pair : scalarTransVars){
			for(std::string varName : pair.second){
				std::string srcLp = pair.first.first;
				std::string destLp = pair.first.second;


				int Inv=0;
				if(srcLp.compare("FUNC_BODY")==0 || destLp.compare("FUNC_BODY")==0){
					Inv=fCalls;
				}
				else{
//					assert(loopInv.find(srcLp)!=loopInv.end());
//					assert(loopInv.find(destLp)!=loopInv.end());
					if(loopInv.find(srcLp)==loopInv.end() || loopInv.find(destLp)!=loopInv.end()){
						Inv = 0;
					}
					else{
						Inv = std::min(loopInv[srcLp],loopInv[destLp]);
					}

				}
//				std::cout << "srcLp=" << srcLp << ",destLp=" << destLp << ",Inv=" << Inv << "Var=" << varName << "\n";
				scalarCommCycles+=(4*Inv/fCalls);
			}
		}
		scalarCommCycles=scalarCommCycles/scalarCommDivisor;
//		std::cout << "scalarCommCycles=" << scalarCommCycles << "\n";

//		std::cout << "Calculating Array Edges...\n";
		float arrCommCycles=0;
		std::map<std::pair<std::string,std::string>,std::set<std::string>> arrTransVars;

		for ( std::pair<std::string,std::map<std::string,std::set<std::string>>> pair1 : arrVarSetEdges){
			std::string srcLp = pair1.first;
			for(std::pair<std::string,std::set<std::string>> pair2 : pair1.second){
				std::string destLp = pair2.first;

				if(loopParition[srcLp]!=loopParition[destLp]){
					for(std::string varName : pair2.second){
						std::string srcLpTrans=srcLp;
						std::string destLpTrans=destLp;

//						std::cout << "srcLp=" << srcLp << ",destLp=" << destLp << "\n";
						if(srcLp.compare("FUNC_BODY")!=0 && destLp.compare("FUNC_BODY")!=0 ){
							if(srcLp.size() > destLp.size()){
								srcLpTrans=srcLp.substr(0,destLp.size());
							}
							else{
								destLpTrans=destLp.substr(0,srcLp.size());
							}
						}
//						std::cout << "srcLpTrans=" << srcLpTrans << ",destLpTrans=" << destLpTrans << "\n";
						arrTransVars[std::make_pair(srcLpTrans,destLpTrans)].insert(varName);
					}
				}
			}
		}

//		std::cout << "calculating inv..\n";

		for(std::pair<std::pair<std::string,std::string>,std::set<std::string>> pair : arrTransVars){
			for(std::string arrName : pair.second){
				std::string srcLp = pair.first.first;
				std::string destLp = pair.first.second;

				int Inv=0;
				if(srcLp.compare("FUNC_BODY")==0 || destLp.compare("FUNC_BODY")==0){
					Inv=fCalls;
				}
				else{
					assert(loopInv.find(srcLp)!=loopInv.end());
					assert(loopInv.find(destLp)!=loopInv.end());
					Inv = std::min(loopInv[srcLp],loopInv[destLp]);
				}
//				std::cout << "srcLp=" << srcLp << ",destLp=" << destLp << ",Inv=" << Inv << ",Arr=" << arrName<< ",Size=" << arrSizes[arrName] <<  "\n";
				arrCommCycles+=(arrSizes[arrName]*Inv/fCalls);
			}
		}
		arrCommCycles=arrCommCycles/arrayCommDivisor;
//		std::cout << "arrCommCycles=" << arrCommCycles << "\n";

		res.commCycles=scalarCommCycles + arrCommCycles;

		//Calculate CGRA Cycles
		int CGRACycles=0;
		int requiredCWs=0;
		for(std::pair<std::string,std::map<std::string,std::pair<int,int>>> pair : loopsForCGRA){
			std::string lp = pair.first;
			std::map<std::string,std::pair<int,int>> tmp232 = pair.second;
			for(std::string munit : Loop2MUnitMap[lp]){
//				int maxBBInv=0;
//				for(std::string BB : MUnit2BBMap[munit]){
//					if(BBInvoMap[BB] > maxBBInv){
//						maxBBInv=BBInvoMap[BB];
//					}
//				}

				std::cout << "munit : " << munit << ",";
				assert(MappableBlocks.find(munit)!=MappableBlocks.end());
//				assert(boxConfig.find(munit)!=boxConfig.end());

				int Xwidth = pair.second[munit].first;
				int Ywidth = pair.second[munit].second;
				std::cout << Xwidth << "," << Ywidth << " calculating...\n";
				assert(MappableBlocks[munit].find(Xwidth)!=MappableBlocks[munit].end());
				assert(MappableBlocks[munit][Xwidth].find(Ywidth)!=MappableBlocks[munit][Xwidth].end());
				int mappedII = MappableBlocks[munit][Xwidth][Ywidth];

				if(MappableBlocks[munit][Xwidth][Ywidth]==0){
					std::cout << "munit : " << munit << "," << Xwidth << "," << Ywidth << " does not exist!\n";
				}
				assert(MappableBlocks[munit][Xwidth][Ywidth]!=0);
				assert(MappableBlocksLatency[munit][Xwidth][Ywidth]!=0);


				int newCycles =(MUnitIterMap[munit] - MUnitInvoMap[munit])*mappedII + MUnitInvoMap[munit]*MappableBlocksLatency[munit][Xwidth][Ywidth];
				CGRACycles+=(newCycles+fCalls-1)/fCalls;
				requiredCWs+=mappedII;
			}
		}
		res.CGRACycles=CGRACycles;
		res.configContexts=requiredCWs;

		//Calculate Host Cycles if these were mapped to host
		int HostCycles=0;
		for(std::pair<std::string,std::map<std::string,std::pair<int,int>>> pair : loopsForCGRA){
			std::string lp = pair.first;
			for(std::string munit : Loop2MUnitMap[lp]){
				for(std::string BB : MUnit2BBMap[munit]){
					HostCycles+=(BBInvoMap[BB])*BBNodeCountMap[BB];
				}
			}
		}
		res.HostCycles=(HostCycles+fCalls-1)/fCalls;
		return res;
}

ResultNMp NestedMapping::calcCyclesType3(std::map<std::string,std::map<std::string,std::pair<int,int>>> loopsForCGRA) {
	ResultNMp res;

	std::vector<std::string> loopsForHost;
	loopsForHost.push_back("FUNC_BODY");
	enum partition {HOST,CGRA};

			 //loopName
	std::map<std::string,partition> loopParition;

	for(std::pair<std::string,std::set<std::string> > pair : Loop2MUnitMap){
		std::string loopName = pair.first;
//		if(std::find(loopsForCGRA.begin(),loopsForCGRA.end(),loopName)==loopsForCGRA.end()){
		if(loopsForCGRA.find(loopName)==loopsForCGRA.end()){
			loopParition[loopName]=HOST;
		}
		else{
			loopParition[loopName]=CGRA;
		}
	}

	//Calculate Comm Cycles
	int scalarCommCycles=0;

	for ( std::pair<std::string,std::map<std::string,int> > pair1 : weightedScalarTransEdges){
		std::string srcLp = pair1.first;
		for(std::pair<std::string,int> pair2 : pair1.second){
			std::string destLp = pair2.first;
			if(loopParition[srcLp]!=loopParition[destLp]){
				scalarCommCycles+=pair2.second/scalarCommDivisor;
			}
		}
	}

	int arrCommCycles=0;
	for ( std::pair<std::string,std::map<std::string,int> > pair1 : weightedArrTransEdges){
		std::string srcLp = pair1.first;
		for(std::pair<std::string,int> pair2 : pair1.second){
			std::string destLp = pair2.first;
			if(loopParition[srcLp]!=loopParition[destLp]){
				arrCommCycles+=pair2.second/arrayCommDivisor;
			}
		}
	}

	res.commCycles=scalarCommCycles + arrCommCycles;

	//Calculate CGRA Cycles
	int CGRACycles=0;
	int requiredCWs=0;
	for(std::pair<std::string,std::map<std::string,std::pair<int,int>>> pair : loopsForCGRA){
		std::string lp = pair.first;

		for(std::string munit : Loop2MUnitMap[lp]){
			int Xwidth = pair.second[munit].first;
			int Ywidth = pair.second[munit].second;
			int maxBBInv=0;
			for(std::string BB : MUnit2BBMap[munit]){
				if(BBInvoMap[BB] > maxBBInv){
					maxBBInv=BBInvoMap[BB];
				}
			}
			int mappedII = MappableBlocks[munit][Xwidth][Ywidth];
			CGRACycles+=(maxBBInv/fCalls)*mappedII;
			requiredCWs+=mappedII;
		}
	}
	res.CGRACycles=CGRACycles;
	res.configContexts=requiredCWs;

	//Calculate Host Cycles if these were mapped to host
	int HostCycles=0;
	for(std::pair<std::string,std::map<std::string,std::pair<int,int>>> pair : loopsForCGRA){
		std::string lp = pair.first;
		for(std::string munit : Loop2MUnitMap[lp]){
			for(std::string BB : MUnit2BBMap[munit]){
				HostCycles+=(BBInvoMap[BB]/fCalls)*BBNodeCountMap[BB];
			}
		}
	}
	res.HostCycles=HostCycles;
	return res;
}

bool NestedMapping::findBestLoops(int XWidth, int YWidth, int ZWidth) {

		std::cout << "findBestLoops STARTED!\n";
		currXWidth=XWidth;
		currYWidth=YWidth;
		currZWidth=ZWidth;

		//store terminal results
		struct terminalRes{
			std::set<std::string> L_plus;
			std::set<std::string> L_minus;
			int savings;
			std::map<std::string,boxDims> mUnitDims;
			std::vector<int> munitComboIdxVec;
			int fCalls;

			ResultNMp cycleCounts;
			OPPSolver oppS;
			bool operator<(const terminalRes& rhs) const
			{
				return savings < rhs.savings;
			}

			void print(){
				int vol = oppS.printEdges();

				std::cout << "munitComboIdxVec : " << munitComboIdxVec.size() << "\n";
				for(int idx : munitComboIdxVec){
					std::cout << idx << ",";
				}
				std::cout << "\n";
				std::cout << "savings=" << savings << "\n";
				std::cout << "HostCycles=" << cycleCounts.HostCycles << "\n";
				std::cout << "CGRACycles=" << cycleCounts.CGRACycles << "\n";
				std::cout << "commCycles=" << cycleCounts.commCycles << "\n";
				std::cout << "context=" << (vol*8/fCalls) << "\n";
				std::cout << "NEW_SAVINGS=" << savings - (vol*8/fCalls) << "\n";
			}
		};


		searchInfoKS init;

		std::priority_queue<terminalRes> resQ;

		//Initialize L_potential
		std::cout << "adding potential loops : ";
		for(std::pair<std::string, std::set<std::string>> pair1 : loopConstraints){
			if(pair1.second.empty()){
				std::cout << pair1.first << ",";
				init.L_potential.insert(pair1.first);
			}
		}
		std::cout << "\n";

		std::stack<searchInfoKS> siQ;
		siQ.push(init);


		while(!siQ.empty()){
			std::cout << "-------------------------------------------------------\n";
			std::cout << "-------------------------------------------------------\n";


			searchInfoKS currSI = siQ.top();
			siQ.pop();

			int munitCount=0;
			for(std::string lp : currSI.L_plus){
				munitCount+=Loop2MUnitMap[lp].size();
			}

			if(munitCount!=currSI.mUnitDims.size()){
				std::cout << "munitCount = " << munitCount ;
				std::cout << ",currSI.mUnitDims.size()=" << currSI.mUnitDims.size() << "\n";
			}
			assert(munitCount==currSI.mUnitDims.size());

			std::cout << "Potential Loop Size = " << currSI.L_potential.size() << "\n";
			for(std::string potLop : currSI.L_potential){
				std::cout << potLop << ",";
			}
			std::cout << "\n";

			//check whether the added loop works, if not just end the decision branch here
			//if the added loop works, populate the potential.
			OPPSolver oppS;
			std::set<node,node_compare> V_new;
			std::map<node,int,node_compare> Wx_new;
			std::map<node,int,node_compare> Wy_new;
			std::map<node,int,node_compare> Wz_new;
			if(currSI.res.oppS.isSearchInfoQEmpty()){
				oppS = createOPPSolver(currSI.mUnitDims,boxDims(XWidth,YWidth,ZWidth));
			}
			else{
				oppS = currSI.res.oppS;
				if(currSI.latestLplus.compare("NA")!=0){
					assert(currSI.latestLplus.compare("NA")!=0);

					int idx=oppS.getVSize()+1;
					for(std::string munit : Loop2MUnitMap[currSI.latestLplus]){
						node n = node(idx);
						n.name=munit;
						V_new.insert(n);
						Wx_new[n]=currSI.mUnitDims[munit].x;
						Wy_new[n]=currSI.mUnitDims[munit].y;
						Wz_new[n]=currSI.mUnitDims[munit].z;
						idx++;
					}
				}
			}

			if(currSI.latestLplus.compare("NA")!=0){
				std::cout << "adding loop : " << currSI.latestLplus << "and checking packing\n";
				std::cout << "munits :";

				int testvol=0;
				for(std::pair<std::string,boxDims> pair1 : currSI.mUnitDims){
					std::cout << "munit:" << pair1.first << "::(";
					std::cout << pair1.second.x << ",";
					std::cout << pair1.second.y << ",";
					std::cout << pair1.second.z << ")\n";
					testvol+=pair1.second.x*pair1.second.y*pair1.second.z;
				}
				std::cout << "Vol = " << testvol << "/" << currXWidth*currYWidth*currZWidth << "\n";
				std::cout << "\n";


				assert(!currSI.mUnitDims.empty());

				//Pre leaf check
				{
					int newLoopsAdded=0;
					for(std::pair<std::string, std::set<std::string>> pair1 : loopConstraints){
						std::string newLoop = pair1.first;

						if(currSI.L_plus.find(newLoop)!=currSI.L_plus.end()){
							continue;
						}
						if(currSI.L_minus.find(newLoop)!=currSI.L_minus.end()){
							continue;
						}
						if(currSI.L_potential.find(newLoop)!=currSI.L_potential.end()){
							continue;
						}

						bool constrainsSatisfied=true;
						for(std::string constrainLoop : pair1.second){
							if(currSI.L_plus.find(constrainLoop)==currSI.L_plus.end()){
								constrainsSatisfied=false;
								break;
							}
						}
						if(constrainsSatisfied){
							newLoopsAdded++;
						}

					}


					if(currSI.L_potential.empty() && (newLoopsAdded==0)){ //this really a leaf node
						ResultNMp tempRes = calcCycles(currSI.L_plus,currSI.mUnitDims);
						std::cout << "curr CGRA cycles = " << tempRes.CGRACycles << "\n";
						if(leafNodeCGRACycles.find(currSI.L_plus)!=leafNodeCGRACycles.end()){
							std::cout << "pre leaf past best soln CGRA cycles = " << leafNodeCGRACycles[currSI.L_plus] << "\n";
							if(tempRes.CGRACycles >= leafNodeCGRACycles[currSI.L_plus]){
								std::cout << "Pre_Leaf check, there is a better leaf solution, we can skip...\n";
								continue;
							}
						}
					}

				}


				bool resultOPP;
				if(currSI.res.oppS.isSearchInfoQEmpty()){
					std::cout << "solveOPP v1\n";
					resultOPP = oppS.solveOPP();
				}
				else{
					std::cout << "solveOPP v2\n";
					resultOPP = oppS.solveOPP_V2(V_new,Wx_new,Wy_new,Wz_new);
				}

				if(resultOPP){
					std::cout << "Solved!\n";
					currSI.res.cycleCounts = calcCycles(currSI.L_plus,currSI.mUnitDims);
					currSI.res.savings = currSI.res.cycleCounts.HostCycles - currSI.res.cycleCounts.CGRACycles - currSI.res.cycleCounts.commCycles;
					currSI.res.oppS=oppS;
//					currSI.res.oppS.clearSearchInfoQ();
					currSI.res.isValid = true;

//					std::cout << "printing edges...\n";
					for(std::pair<std::string, std::set<std::string>> pair1 : loopConstraints){
						std::string newLoop = pair1.first;

						if(currSI.L_plus.find(newLoop)!=currSI.L_plus.end()){
							continue;
						}
						if(currSI.L_minus.find(newLoop)!=currSI.L_minus.end()){
							continue;
						}
						if(currSI.L_potential.find(newLoop)!=currSI.L_potential.end()){
							continue;
						}

						bool constrainsSatisfied=true;
						for(std::string constrainLoop : pair1.second){
							if(currSI.L_plus.find(constrainLoop)==currSI.L_plus.end()){
								constrainsSatisfied=false;
								break;
							}
						}

						if(constrainsSatisfied){
							std::cout << "adding potential loop : " << newLoop << "\n";
							currSI.L_potential.insert(newLoop);
						}
					}

					if(currSI.L_potential.empty()){ //leaf node
						if(leafNodeCGRACycles.find(currSI.L_plus)!=leafNodeCGRACycles.end()){
							if(currSI.res.cycleCounts.CGRACycles < leafNodeCGRACycles[currSI.L_plus]){
								leafNodeCGRACycles[currSI.L_plus] = currSI.res.cycleCounts.CGRACycles;
							}
						}
						else{
							leafNodeCGRACycles[currSI.L_plus] = currSI.res.cycleCounts.CGRACycles;
						}
					}
				}
				else{
					std::cout << "Packing failed!.\n";
//					std::cout << "packing Result of the parent : \n";
//					std::cout << "L.plus : ";
//					for (std::string lp : currSI.L_plus){
//						std::cout << lp << ",";
//					}
//					std::cout << "\n";
//					currSI.res.oppS.printEdges();
//					std::cout << "savings=" << currSI.res.savings << "\n";
//					std::cout << "HostCycles=" << currSI.res.cycleCounts.HostCycles << "\n";
//					std::cout << "CGRACycles=" << currSI.res.cycleCounts.CGRACycles << "\n";
//					std::cout << "commCycles=" << currSI.res.cycleCounts.commCycles << "\n";
//
//					terminalRes res;
//					res.L_plus=currSI.L_plus;
//					res.mUnitDims=currSI.mUnitDims;
//					res.savings=currSI.res.savings;
//					res.L_minus=currSI.L_minus;
//					res.cycleCounts = currSI.res.cycleCounts;
//					res.oppS=currSI.res.oppS;
//					res.munitComboIdxVec=currSI.munitComboIdxVec;
//					resQ.push(res);
//
//					if(!resQ.empty()){
//						std::cout << "****BEST SO FAR**** \n";
//						terminalRes best = resQ.top();
//						best.print();
//						std::cout << "******************* \n";
//					}
					continue;
				}
			}


//			if(currSI.L_potential.empty()){
			if(!currSI.L_plus.empty()){
				ResultNMp resCycles = calcCycles(currSI.L_plus,currSI.mUnitDims);
				int savings = resCycles.HostCycles - resCycles.CGRACycles - resCycles.commCycles;
				terminalRes res;
				res.L_plus=currSI.L_plus;
				res.mUnitDims=currSI.mUnitDims;
				res.savings=savings;
				res.L_minus=currSI.L_minus;
				res.cycleCounts = resCycles;
				res.oppS=oppS;
				res.oppS.clearSearchInfoQ();
				res.munitComboIdxVec=currSI.munitComboIdxVec;
				res.fCalls=fCalls;

				std::cout << "Curr L.plus : ";
				for (std::string lp : currSI.L_plus){
					std::cout << lp << ",";
				}
				std::cout << "\n";
				oppS.printEdges();
				std::cout << "savings=" << savings << "\n";
				std::cout << "HostCycles=" << resCycles.HostCycles << "\n";
				std::cout << "CGRACycles=" << resCycles.CGRACycles << "\n";
				std::cout << "commCycles=" << resCycles.commCycles << "\n";
				resQ.push(res);

				if(!resQ.empty()){
					std::cout << "****BEST SO FAR**** \n";
					terminalRes best = resQ.top();
					best.print();
					std::cout << "******************* \n";
				}
			}




			std::vector<std::string> potLoops = sortPotLoop(currSI);
			for(std::string potLp : potLoops){
				std::map<std::string,std::vector<boxDims>> mUnitDims;
				//adding negation
				searchInfoKS siNextMinus = currSI;
				siNextMinus.L_potential.erase(potLp);
				siNextMinus.L_minus.insert(potLp);
				siNextMinus.latestLplus = "NA";
				std::cout << potLp << "-\n";
				siQ.push(siNextMinus);


				//adding plus
				for(std::string munit : Loop2MUnitMap[potLp]){
					for(std::pair<int, std::map<int,int>> pair1 : MappableBlocks[munit]){
						for( std::pair<int,int> pair2 : pair1.second){
							mUnitDims[munit].push_back(boxDims(pair1.first,pair2.first,pair2.second));
						}
					}
				}

				std::vector<std::map<std::string,boxDims>> allBoxCombs;
				std::map<std::string,boxDims> currBoxComb;
				getmUnitCombinations(mUnitDims,mUnitDims.begin(),currBoxComb,&allBoxCombs);

				std::cout << potLp << "+\n";

				struct searchInfoVol{
					searchInfoKS si;
					int vol;
					int z;
					float hostDensity;
					int CGRACycles;
					bool operator<(const searchInfoVol& other){
						return vol < other.vol;
					}
					bool operator>(const searchInfoVol& other){
						return vol > other.vol;
					}
					searchInfoVol(searchInfoKS si,int vol, int z, float hostDensity, int CGRACycles):si(si),vol(vol),z(z), hostDensity(hostDensity), CGRACycles(CGRACycles){};
				};

				struct {
					bool operator()(const searchInfoVol& l, const searchInfoVol& r){
						return l.z > r.z || ((l.z == r.z) && l.vol > r.vol);
					}
				}searchInfoVol_dec;


				struct {
					bool operator()(const searchInfoVol& l, const searchInfoVol& r){
						return l.z < r.z || ((l.z == r.z) && l.vol < r.vol);
					}
				}searchInfoVol_inc;

				struct {
					bool operator()(const searchInfoVol& l, const searchInfoVol& r){
						return (l.hostDensity < r.hostDensity) ||
							   ((l.hostDensity == r.hostDensity) && l.z > r.z) ||
							   ((l.hostDensity == r.hostDensity) && (l.z == r.z) && (l.vol > r.vol));
					}
				}searchInfoVolHostDensity_inc;

				struct {
					bool operator()(const searchInfoVol& l, const searchInfoVol& r){
						return l.CGRACycles > r.CGRACycles || ((l.CGRACycles == r.CGRACycles) && l.vol > r.vol);
					}
				}searchInfoVolCGRACyc_dec;



				std::vector<searchInfoVol> searchInfoVolVec;
				for(std::map<std::string,boxDims> boxConfig : allBoxCombs){
					searchInfoKS siNextPlus = currSI;
					siNextPlus.L_potential.erase(potLp);


					int vol=0;
					int zsum=0;
					int CGRACycles=0;
					float hostDensity=0;
					for(std::pair<std::string,boxDims> pair : boxConfig){
						std::string munit = pair.first;
						int newCGRACycles =  (MUnitIterMap[munit] - MUnitInvoMap[munit])*pair.second.z + MUnitInvoMap[munit]*MappableBlocksLatency[munit][pair.second.x][pair.second.y];
						CGRACycles += newCGRACycles/fCalls;

						siNextPlus.mUnitDims[pair.first]=pair.second;
						vol+=pair.second.x*pair.second.y*pair.second.z;
						zsum+=pair.second.z;

						float HostCycles=0;
						float currVol=pair.second.x*pair.second.y*pair.second.z;
						for(std::string BB : MUnit2BBMap[pair.first]){
							HostCycles=(BBInvoMap[BB]/fCalls)*BBNodeCountMap[BB];
						}

						int maxBBInv=0;
						for(std::string BB : MUnit2BBMap[pair.first]){
							if(BBInvoMap[BB] > maxBBInv){
								maxBBInv=BBInvoMap[BB];
							}
						}
						float CGRACycles=(maxBBInv/fCalls)*pair.second.z;
						hostDensity+=(HostCycles-CGRACycles)/currVol;
					}

					int aggrVol=0;
					for(std::pair<std::string,boxDims> pair1 : siNextPlus.mUnitDims){
						aggrVol+=pair1.second.x*pair1.second.y*pair1.second.z;
					}
					if(aggrVol > currXWidth*currYWidth*currZWidth){
						std::cout << "Aggr Vol is larger....\n";
						continue;
					}

					for(std::string munit : Loop2MUnitMap[potLp]){
						if(siNextPlus.mUnitDims.find(munit)==siNextPlus.mUnitDims.end()){
							std::cout << "potLp = " << potLp << ",munit=" << munit << "\n";
						}
						assert(siNextPlus.mUnitDims.find(munit)!=siNextPlus.mUnitDims.end());
					}

					siNextPlus.L_plus.insert(potLp);
					siNextPlus.latestLplus = potLp;
					searchInfoVolVec.push_back(searchInfoVol(siNextPlus,vol,zsum,hostDensity,CGRACycles));
//					siQ.push(siNextPlus);
				}

//				assert(fCalls==12);

				std::vector<searchInfoVol> searchInfoVolVecCopy;

				if(strgy==CGRA_CYCLES){
					std::sort(searchInfoVolVec.begin(),searchInfoVolVec.end(),searchInfoVolCGRACyc_dec);
				}
				else if(strgy==MIXED){
					searchInfoVolVecCopy = searchInfoVolVec;
					std::sort(searchInfoVolVec.begin(),searchInfoVolVec.end(),searchInfoVolCGRACyc_dec);
					std::sort(searchInfoVolVecCopy.begin(),searchInfoVolVecCopy.end(),searchInfoVolHostDensity_inc);
				}
				else{
					assert(strgy==GAIN_DENSITY);
					std::sort(searchInfoVolVec.begin(),searchInfoVolVec.end(),searchInfoVolHostDensity_inc);
				}

//				std::sort(searchInfoVolVec.begin(),searchInfoVolVec.end(),searchInfoVolHostDensity_inc);

				if(strgy != MIXED){
					int idx=searchInfoVolVec.size()-1;
					int prevVol=-1;
					for(searchInfoVol siVol : searchInfoVolVec){
#ifdef SUBOPTIMAL
						if(idx >= 5){
							idx--;
							continue;
						}
#endif
						siVol.si.munitComboIdxVec.push_back(idx);
						siQ.push(siVol.si);
						idx--;
						prevVol = siVol.vol;
					}
					std::cout << "added combos : " << (searchInfoVolVec.size()-1-idx) << "\n";
				}
				else{
					int first;
					int second;

					{
						int idx=searchInfoVolVec.size()-1;
						int prevVol=-1;
						for(searchInfoVol siVol : searchInfoVolVec){

							if(idx >= 1){
								idx--;
								continue;
							}

							siVol.si.munitComboIdxVec.push_back(idx);
							siQ.push(siVol.si);
							idx--;
							prevVol = siVol.vol;
						}
						first = searchInfoVolVec.size() -1 - idx;
					}

					{
						int idx=searchInfoVolVecCopy.size()-1;
						int prevVol=-1;
						for(searchInfoVol siVol : searchInfoVolVecCopy){

							if(idx >= 1){
								idx--;
								continue;
							}

							siVol.si.munitComboIdxVec.push_back(idx);
							siQ.push(siVol.si);
							idx--;
							prevVol = siVol.vol;
						}
						second = searchInfoVolVecCopy.size()-1-idx;
					}

					std::cout << "added combos : " << (first+second) << "\n";
				}

			}

			std::cout << "-------------------------------------------------------\n";
		}

		std::cout << "Best Conf : \n";
		if(!resQ.empty()){
			terminalRes bestSoln = resQ.top();
			for (std::pair<std::string,boxDims> pair : bestSoln.mUnitDims){
				std::cout << "munit:" <<  pair.first;
				std::cout << ",x=" << pair.second.x;
				std::cout << ",y=" << pair.second.y;
				std::cout << ",z=" << pair.second.z;
				std::cout << ",save=" << bestSoln.savings << "\n";
			}
		}
		else{
			std::cout << "Result is empty.\n";
		}

		std::cout << "findBestLoops ENDED!\n";
		return true;
}

void NestedMapping::populateLoopConstraints() {

	loopConstraints.clear();
	for ( std::pair<std::string,std::set<std::string>> pair1 : Loop2MUnitMap){
		std::string currLoopStr = pair1.first;
		for( std::pair<std::string, std::set<std::string>> pair2 : loopConstraints){
			std::string oldStr = pair2.first;
			if(currLoopStr.find(oldStr)!= std::string::npos){
				loopConstraints[oldStr].insert(currLoopStr);
			}
			if(oldStr.find(currLoopStr)!= std::string::npos){
				loopConstraints[currLoopStr].insert(oldStr);
			}
		}

		if(loopConstraints.find(currLoopStr)==loopConstraints.end()){
			std::set<std::string> emptySet;
			loopConstraints[currLoopStr]=emptySet;
		}

	}

	std::cout << "Printing Loop Constraints : \n";
	for( std::pair<std::string, std::set<std::string>> pair1 : loopConstraints){
		std::cout << pair1.first << " :: ";
		for(std::string depStr : pair1.second){
			std::cout << depStr << ",";
		}
		std::cout << "\n";
	}
}

void NestedMapping::getmUnitCombinations(
		std::map<std::string,std::vector<boxDims>>&  mUnitDims,
		std::map<std::string,std::vector<boxDims>>::iterator  mUnitDimsIT,
		std::map<std::string,boxDims> currBoxComb,
		std::vector<std::map<std::string,boxDims>>* prevCombs) {

	if(mUnitDimsIT == mUnitDims.end()){
		prevCombs->push_back(currBoxComb);
		return;
	}

	std::string currMunit = mUnitDimsIT->first;

	for(boxDims box : mUnitDims[currMunit]){
		currBoxComb[currMunit]=box;
		getmUnitCombinations(mUnitDims,std::next(mUnitDimsIT),currBoxComb,prevCombs);
	}
}

OPPSolver NestedMapping::createOPPSolver(
		std::map<std::string, boxDims> boxConfig, boxDims Container) {

	int idx=1;
	std::map<node,int,node_compare> Wx;
	std::map<node,int,node_compare> Wy;
	std::map<node,int,node_compare> Wz;
	std::set<node,node_compare> V;

	std::cout << "OPP number of boxes : " << boxConfig.size() << "\n";

	for(std::pair<std::string, boxDims> pair : boxConfig){
		node n(idx);
		n.name=pair.first;
		V.insert(n);
		Wx[n]=pair.second.x;
		Wy[n]=pair.second.y;
		Wz[n]=pair.second.z;
		idx++;
	}
	OPPSolver oppsolverIns(V,Wx,Wy,Wz);
	oppsolverIns.setLimits(Container.x,Container.y,Container.z);
	return oppsolverIns;
}

ResultNMp NestedMapping::calcCycles(std::set<std::string> L_plus, std::map<std::string,boxDims> mUnitDims) {
	std::map<std::string,std::map<std::string,std::pair<int,int>>> loopsForCGRA;
	int volSum=0;
	for(std::string lp : L_plus){
		for(std::string munit : Loop2MUnitMap[lp]){
			std::cout << "lp :" << lp << ",munit=" << munit;
			assert(mUnitDims.find(munit)!=mUnitDims.end());
			boxDims currbDim = mUnitDims[munit];
			int x = currbDim.x;
			int y = currbDim.y;
			int z = currbDim.z;

			volSum+=x*y*z;

			std::cout << ",(" << x << "," << y << "," << z << ")";
			std::cout << ",accVol=" << volSum << "\n";
			loopsForCGRA[lp][munit]=std::make_pair(x,y);
		}
	};

	if(volSum > currXWidth*currYWidth*currZWidth){
		std::cout << "volSum = " << volSum << "," << "ContainedVol=" << currXWidth*currYWidth*currZWidth << "\n";
	}

	assert(volSum <= currXWidth*currYWidth*currZWidth);
	return calcCycles(loopsForCGRA);
}

void NestedMapping::insertLoopISODim(std::string loopName,
		std::map<std::string, std::map<std::string, std::pair<int, int> > >* loopsForCGRAPtr,
		int XDim, int YDim) {

	assert(XDim!=0);
	assert(YDim!=0);

	for(std::string munit : Loop2MUnitMap[loopName]){
		(*loopsForCGRAPtr)[loopName][munit]=std::make_pair(XDim,YDim);
	}
}

std::vector<std::string> NestedMapping::sortPotLoop(searchInfoKS currSI) {

	struct loopSaving{
		std::string L;
		int saving;
	};

	struct{
		bool operator()(const loopSaving l, const loopSaving r){
			return l.saving < r.saving;
		}
	}loopSaving_cmp;

	std::vector<loopSaving> tmp;
	std::map<std::string, std::map<std::string, std::pair<int, int> >> loopsForCGRAInit;

	for(std::string L : currSI.L_plus){
		for(std::string munit : Loop2MUnitMap[L]){
			assert(currSI.mUnitDims.find(munit)!=currSI.mUnitDims.end());
			boxDims b = currSI.mUnitDims[munit];

			assert(b.x!=0);
			assert(b.y!=0);

			loopsForCGRAInit[L][munit]=std::make_pair(b.x,b.y);
		}

//		for(std::pair<std::string,boxDims> pair : currSI.mUnitDims){
//			std::string munit = pair.first;
//			boxDims b = pair.second;
//			loopsForCGRAInit[L][munit]=std::make_pair(b.x,b.y);
//		}
	}



	for(std::string L : currSI.L_potential){
		std::map<std::string, std::map<std::string, std::pair<int, int> >> loopsForCGRA;// = loopsForCGRAInit;
		loopsForCGRA = loopsForCGRAInit;
		insertLoopISODim(L,&loopsForCGRA,currXWidth,currYWidth);
		ResultNMp res = calcCycles(loopsForCGRA);
		int savings = res.HostCycles - res.CGRACycles - res.commCycles;
		loopSaving currLS;
		currLS.L=L;
		currLS.saving=savings;
		tmp.push_back(currLS);
	}

	std::sort(tmp.begin(),tmp.end(),loopSaving_cmp);

	std::vector<std::string> result;

	for(loopSaving LS : tmp){
		std::cout << "L=" << LS.L << "," << ",Sav=" << LS.saving << "\n";
		result.push_back(LS.L);
	}

	return result;
}
